package com.grp.cokreates.security.resource.api;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

import com.grp.cokreates.security.resource.Service.PostService;
import com.grp.cokreates.security.resource.Service.UserService;


public class CustomMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {
	
	private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

	private ApplicationContext applicationContext;
	
	
	@Override
	public MethodSecurityExpressionOperations createSecurityExpressionRoot( Authentication authentication,
			MethodInvocation invocation) {
		CustomMethodSecurityExpressionRoot root = new CustomMethodSecurityExpressionRoot(authentication);
		root.setPermissionEvaluator(getPermissionEvaluator());
		root.setTrustResolver(this.trustResolver);
		root.setRoleHierarchy(getRoleHierarchy());
		root.setUserService(applicationContext.getBean(UserService.class));
		root.setPostService(applicationContext.getBean(PostService.class));
		return root;
	}
	
	 @Override
	public void setApplicationContext(ApplicationContext applicationContext){
	        super.setApplicationContext(applicationContext);
	        this.applicationContext = applicationContext;
	        
	    }
}
