package com.grp.cokreates.security.resource.api;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.grp.cokreates.security.resource.Service.PostService;
import com.grp.cokreates.security.resource.Service.UserService;


public class CustomMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

	

	private UserService userService;
	

	private PostService postService;
	
//	@Autowired
//	OAuth2Authentication oauth2Authentication;
	
	private Authentication auth;
	
  public CustomMethodSecurityExpressionRoot( Authentication authentication) {
      super(authentication);
      this.auth=authentication;
      
  }

  public boolean hasPermission(String method) {
      String  userId = (String) this.getPrincipal();
      
      System.out.println(userId);
      
      Authentication a = SecurityContextHolder.getContext().getAuthentication();

      String clientId = ((OAuth2Authentication) a).getOAuth2Request().getClientId();  // .getAuthorizationRequest().getClientId();

      System.out.println("Client-----"+clientId);
      
      
      
      RestTemplate restTemplate = new RestTemplate();

		String url = "http://localhost:8011/auth-ws/authorize/method/{method}";
		
		System.out.println(url);
		
		
		
		try {

			restTemplate.exchange(url, HttpMethod.GET,
					new HttpEntity<String>(createHeaders(userId,clientId)), String.class, method);

		} catch (HttpStatusCodeException e) {
			System.out.println("caught");
			  
			return false;
		}
      
//      
//      List<Post> posts=userService.findByUserid(id);
//      
//      System.out.println("passed");
//      for(Post post: posts)
//      {
//    	  System.out.println(post.getPostid());
//    	  
//    	  List<Api> apis=postService.findByPostId(post.getPostid());
//    	  System.out.println("passed");
//    	  for(Api api: apis)
//    	  {
//    		  if(api.getApiname().matches(apiName))
//    		  {
//    			  System.out.println("found");
//    			  return true;
//    		  }
//    		  
//    	  }
//    	  
//      }
//      return false;
	  
	  
	  
	  return true;
	  

}
  
  
  public boolean AclPermission(Object target, Object permission)
{
	  System.out.println("Came");
	  System.out.println(target.toString().substring(target.toString().indexOf("id")+3, target.toString().indexOf("id")+4)+"    "+auth.getName()+"   "+ permission);
	  
	  //String object=target.getClass().getName().substring(target.getClass().getName().indexOf("entity")+7);
	  String object="Posdst";
	  
	  System.out.println(object);
	  
	  String objectId=target.toString().substring(target.toString().indexOf("id")+4, target.toString().indexOf("id")+5);
	  
	  String user=auth.getName();
	  
	  RestTemplate restTemplate = new RestTemplate();

		String url = "http://localhost:8011/auth-ws/authorize/acl";
		
		System.out.println(url);
		
		
		
		try {

			restTemplate.exchange(url, HttpMethod.GET,
					new HttpEntity<String>(createHeadersAcl(object,user,objectId,permission.toString())), String.class);

		} catch (HttpStatusCodeException e) {
			System.out.println("caught");
			  
			return false;
		}
		
	  return true;
}
  
  public HttpHeaders createHeaders(String userId,String clientId) {
		return new HttpHeaders() {
			{

				set("userId", userId);
				set("clientId", clientId);
			}
		};
	}
  
  public HttpHeaders createHeadersAcl(String object,String user,String objectId,String permission) {
		return new HttpHeaders() {
			{

				set("object", object);
				set("user", user);
				set("objectId", objectId);
				set("permission",permission);
			}
		};
	}
  
 public void setUserService(UserService userService){
      this.userService = userService;
  }
 public void setPostService(PostService postService) {
	this.postService = postService;
}

@Override
public void setFilterObject(Object filterObject) {
	// TODO Auto-generated method stub
	
}

@Override
public Object getFilterObject() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void setReturnObject(Object returnObject) {
	// TODO Auto-generated method stub
	
}

@Override
public Object getReturnObject() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Object getThis() {
	// TODO Auto-generated method stub
	return null;
}

}
