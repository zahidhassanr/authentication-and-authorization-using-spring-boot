package com.grp.cokreates.security.resource.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.cokreates.security.resource.entity.Api;


public interface ApiRepository extends JpaRepository<Api, Long>{
	
	
	

}
