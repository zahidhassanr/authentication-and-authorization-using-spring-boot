package com.grp.cokreates.security.resource.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.resource.Repository.UserRepository;
import com.grp.cokreates.security.resource.entity.Post;
import com.grp.cokreates.security.resource.entity.User;


@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
//	public List<Post> findByUserid(String userid)
//	{
//
//		User user= userRepository.findByUserid(userid);
//		
//		return user.getPosts();
//
//	}
	
	
	@PreAuthorize("hasPermission('createEmployee')")
	public User create(User user) {
		return userRepository.save(user);
	}

}
