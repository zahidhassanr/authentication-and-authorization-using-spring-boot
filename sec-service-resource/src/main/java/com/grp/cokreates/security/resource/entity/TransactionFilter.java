package com.grp.cokreates.security.resource.entity;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
//import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.RestTemplate;

@Component
@Order(1)
public class TransactionFilter implements Filter {

	@Autowired
	Environment environment;

//	@Autowired
////	@LoadBalanced
//	private RestTemplate restTemplate;

	
	  public TransactionFilter() { }
	 

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		System.out.println("Method" + req.getMethod());

		if (!req.getMethod().matches("POST")) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid request");
			chain.doFilter(request, res);
			return;
		}

		System.out.println("filter");
		String token = req.getHeader("Authorization").replace("Bearer ", "");
		System.out.println("---Token-------" + token);
		String apiName = req.getRequestURI().replace("/api/", "");
		System.out.println("URL" + apiName);

		RestTemplate restTemplate = new RestTemplate();

		String url = "http://localhost:8011/auth-ws/authorize/{api}";
		
		System.out.println(url);
		
		
		// = "http://localhost:8080/authorize/{api}";

//		  HttpHeaders headers = new HttpHeaders();
//		 // headers.set("Authorization", "Bearer "+token);
//		  headers.setContentType(MediaType.TEXT_PLAIN);
//		  
//		   HttpEntity<String> entity = new HttpEntity<String>("Authorization: Bearer "+token, headers);

//		  Map<String, String> params = new HashMap<String, String>();
//		    params.put("api", apiName);

		// System.out.println(entity.toString());

//		  ResponseEntity<String> response1
//		    = restTemplate.exchange(url, HttpMethod.GET, entity, String.class,apiName);

		try {

			restTemplate.exchange(url, HttpMethod.GET,
					new HttpEntity<String>(createHeaders(req.getHeader("Authorization"))), String.class, apiName);

		} catch (HttpStatusCodeException e) {
			System.out.println("caught");
//			  
			System.out.println(e.getStatusCode().value());
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
			// chain.doFilter(request, res);
			return;
		}

		// System.out.println(response1);

		chain.doFilter(request, res);

	}

	HttpHeaders createHeaders(String authHeader) {
		return new HttpHeaders() {
			{

				set("Authorization", authHeader);
			}
		};
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	// other methods
}
