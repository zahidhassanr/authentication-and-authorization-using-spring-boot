package com.grp.cokreates.security.resource.api;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
//import com.grp.cokreates.security.resource.config.CustomPermission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.grp.cokreates.security.resource.Service.PostService;
import com.grp.cokreates.security.resource.Service.UserService;
import com.grp.cokreates.security.resource.entity.Post;
import com.grp.cokreates.security.resource.entity.TransactionFilter;
import com.grp.cokreates.security.resource.entity.User;


@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PostService postService;
	
//	@Autowired
//	private MutableAclService mutableAclService;
	
	//private CustomPermission customPermission;
	
	
	@Bean
	public FilterRegistrationBean<TransactionFilter> loggingFilter(){
	    FilterRegistrationBean<TransactionFilter> registrationBean 
	      = new FilterRegistrationBean<>();
	         
	    registrationBean.setFilter(new TransactionFilter());
	    registrationBean.addUrlPatterns("/api/**");
	         
	    return registrationBean;    
	}

	//@PreAuthorize("hasPermission('add_employee')")
    @RequestMapping(value="/api/add_employee")
    public ResponseEntity<UserProfile> myProfile() {
        String username = (String) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        
       
        String email = username + "added employee";
        
        userService.create(new User("6","Himel","himel@cokreates.com","$2a$10$RmzPWqb1bQCq1ZOqwjJ3TedY/g8YkNs/nWVrRaBzOX96vKX7XeUEu"));

        UserProfile profile = new UserProfile(username, email);

        return ResponseEntity.ok(profile);
    }
	
	//@PreAuthorize("isMember('add_lead')")
    @RequestMapping("/api/add_lead")
    public ResponseEntity<UserProfile> lead() {
        String username = (String) SecurityContextHolder.getContext()
                .getAuthentication().getName();
        String email = username + " added lead";

        UserProfile profile = new UserProfile(username, email);

        return ResponseEntity.ok(profile);
    }
    
    @PostMapping("/acl/posts")
    //@PostAuthorize("hasPermission(returnObject,'read_write')")
    @PreAuthorize("AclPermission(#post,'read_write')")
    public void getPosts(@RequestBody JsonNode post) {

    //	System.out.println("in");
      // Post post=postService.findOne((long)3);
    	
    	
  //  	User user=userService.findByUserid("1");

    	
   //    System.out.println(post.getPostname()+post.getId());
   // 	System.out.println(user.getPublicname());
       
       
//       Serializable objectIds=post.getId();
//		
//		ObjectIdentity oid = new ObjectIdentityImpl(post.getClass(), objectIds);
//		
//		
////		PrincipalSid p= new PrincipalSid(user);
////		List<AccessControlEntry> m;
////		
////		List<Sid> sids=new ArrayList<Sid>();
////		sids.add(p);
//		
//		System.out.println(mutableAclService.readAclById(oid));
//       
//		MutableAcl acl=(MutableAcl) mutableAclService.readAclById(oid);
//		
//		List<AccessControlEntry> entry=acl.getEntries();
//		
//		System.out.println("entry---"+entry);
//		
//		Acl acls=(MutableAcl) mutableAclService.readAclById(oid);
//       
//		Permission permission=customPermission.READ_WRITE;
//		
//		System.out.println("permission"+permission.getMask());
//		
//		List<Permission> ps = new ArrayList<Permission>();
//		
//		ps.add(permission);
//		
//		PrincipalSid p= new PrincipalSid(SecurityContextHolder.getContext().getAuthentication().getName());
//		
//		List<Sid> sids = new ArrayList<Sid>();
//		sids.add(p);
//		try {
//		System.out.println("Gratns"+acl.isGranted(ps, sids, true));
//		}
//		catch(NotFoundException n)
//		{
//			System.out.println("Not found");
//		}
     //  return post;
    }
    
	
//	@RequestMapping("/api")
//	public ResponseEntity<List<Post>> api()
//	{
//		String id = (String) SecurityContextHolder.getContext()
//                .getAuthentication().getPrincipal();
//
//		return new ResponseEntity<List<Post>>(userService.findByUserid(id), HttpStatus.OK);
//		
//
//	}
	

}