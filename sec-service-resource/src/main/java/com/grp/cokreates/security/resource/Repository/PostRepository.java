package com.grp.cokreates.security.resource.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.grp.cokreates.security.resource.entity.Post;
import com.grp.cokreates.security.resource.entity.User;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>{
	
	@Query("select p from Post p where p.id =:postid")
	Post findByPostid(@Param(value="postid") Long postid);

}
