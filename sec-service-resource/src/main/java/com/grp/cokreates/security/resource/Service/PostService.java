package com.grp.cokreates.security.resource.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.resource.Repository.PostRepository;
import com.grp.cokreates.security.resource.entity.Api;
import com.grp.cokreates.security.resource.entity.Post;


@Service
public class PostService{
	
	@Autowired
	private PostRepository postRepository;

	public PostService(PostRepository postRepository) {
		this.postRepository = postRepository;
	}
	
//	public List<Api> findByPostId(Long postid)
//	{
//		return postRepository.findByPostid(postid).getApis();
//	}
	
	public Post findById(Long postid)
	{
		return postRepository.findByPostid(postid);
	}
	
	public Post findOne(Long postid)
	{
		return postRepository.findByPostid(postid);
	}
}
