package com.grp.cokreates.security.resource.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.acls.model.ObjectIdentity;



@Entity
@Table(name="acl_object_identity")
public class AclObjectIdentity implements ObjectIdentity{
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="object_id_class")
    private AclClass objIdClass;

    @Column(name="object_id_identity")
    private Long objIdIdentity;

    @ManyToOne
    @JoinColumn(name="parent_object")
    private AclObjectIdentity parentObject;

    @ManyToOne
    @JoinColumn(name="owner_sid")
    private AclSid owner;

    @Column(name="entries_inheriting")
    private Boolean entriesInheriting;

    @OneToMany(mappedBy="aclObjectIdentity",fetch=FetchType.EAGER)
    private List<AclEntry> aclEntries;

    public List<AclEntry> getAclEntries() {
        return aclEntries;
    }

    public void setAclEntries(List<AclEntry> aclEntries) {
        this.aclEntries = aclEntries;
    }

    public List<AclEntry> getEntries() {
        if(aclEntries!=null) {
            return aclEntries;
        }
        return null;
    }

    public void setEntries(List<AclEntry> entries) {
        this.aclEntries = entries;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AclClass getObjIdClass() {
        return objIdClass;
    }

    public void setObjIdClass(AclClass objIdClass) {
        this.objIdClass = objIdClass;
    }

    public Long getObjIdIdentity() {
        return objIdIdentity;
    }

    public void setObjIdIdentity(Long objIdIdentity) {
        this.objIdIdentity = objIdIdentity;
    }

    public AclObjectIdentity getParentObject() {
        return parentObject;
    }

    public void setParentObject(AclObjectIdentity parentObject) {
        this.parentObject = parentObject;
    }

    public AclSid getOwner() {
        return owner;
    }

    public void setOwner(AclSid owner) {
        this.owner = owner;
    }

    public Boolean getEntriesInheriting() {
        return entriesInheriting;
    }

    public void setEntriesInheriting(Boolean entriesInheriting) {
        this.entriesInheriting = entriesInheriting;
    }

    @Override
    public Serializable getIdentifier() {

        return this.objIdIdentity;
    }

    @Override
    public String getType() {

        return objIdClass.getClazz();
    }

}

