package com.grp.cokreates.security.resource.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;




@Entity
@Table(name="post")
public class Post implements Serializable{

	@Id
	@Column(name="post_id")
	private Long id;
	
	@Column(name="post_name")
	private String postname;

//	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//	@JsonIgnore
//	@ManyToMany(fetch=FetchType.LAZY,
//			cascade= CascadeType.ALL)
//	@JoinTable(
//			name="post_api",
//			joinColumns=@JoinColumn(name="post_id"),
//			inverseJoinColumns=@JoinColumn(name="api_id")
//			)
//	private List<Api> apis;
	
	
//	@ManyToMany(fetch=FetchType.LAZY,
//			cascade= {CascadeType.PERSIST, CascadeType.MERGE,
//			 CascadeType.DETACH, CascadeType.REFRESH})
////	@JoinTable(
////			name="user_post",
////			joinColumns=@JoinColumn(name="post_id"),
////			inverseJoinColumns=@JoinColumn(name="user_id")
////			)
//	private List<User> users;
	
//	public List<Api> getApis() {
//		return apis;
//	}
//
//	public void setApis(List<Api> apis) {
//		this.apis = apis;
//	}

	
	public Long getId()
	{
		//return Long.parseLong(this.id);
		return this.id;
	}
	
	public Long getid() {
		return id;
	}

	public void setid(Long id) {
		this.id = id;
	}

	public String getPostname() {
		return postname;
	}

	public void setPostname(String postname) {
		this.postname = postname;
	}

	public Post(Long id, String postname) {
		super();
		this.id = id;
		this.postname = postname;
	}
	
	public Post()
	{
		
	}
	

   

	
}
