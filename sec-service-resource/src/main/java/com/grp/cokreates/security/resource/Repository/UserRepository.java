package com.grp.cokreates.security.resource.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.grp.cokreates.security.resource.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u where u.userid =:userid")
	User findByUserid(@Param(value="userid") String userid);
	
}
