package com.grp.cokreates.security.resource.oauth;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableResourceServer
//@Import(AclContext.class)
public class OAuth2ResourceServer extends
    ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .anyRequest().authenticated().and()
            .requestMatchers().antMatchers("/**","/acl/**");
        
//        http
//        .authorizeRequests()
//        .anyRequest().permitAll();
    	
//    	http
//		.antMatcher("/api/**")
//		.authorizeRequests()
//			.antMatchers("/api/**").access("#oauth2.hasScope('read_profile')");
//    	 http
//         .csrf().disable()
//         .anonymous().disable()
//         .httpBasic().disable()
//         .cors().disable()
//         .authorizeRequests()
//             .anyRequest().authenticated();
    }
    
//    @Bean
//    @LoadBalanced
//    RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
    
   
}