//package com.grp.cokreates.security.resource.config;
//
//import java.beans.PropertyVetoException;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.ehcache.EhCacheFactoryBean;
//import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
//import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
//import org.springframework.security.acls.AclPermissionCacheOptimizer;
//import org.springframework.security.acls.AclPermissionEvaluator;
//import org.springframework.security.acls.domain.AclAuthorizationStrategy;
//import org.springframework.security.acls.domain.AclAuthorizationStrategyImpl;
//import org.springframework.security.acls.domain.ConsoleAuditLogger;
//import org.springframework.security.acls.domain.DefaultPermissionFactory;
//import org.springframework.security.acls.domain.DefaultPermissionGrantingStrategy;
//import org.springframework.security.acls.domain.EhCacheBasedAclCache;
//import org.springframework.security.acls.jdbc.BasicLookupStrategy;
//import org.springframework.security.acls.jdbc.JdbcMutableAclService;
//import org.springframework.security.acls.jdbc.LookupStrategy;
//import org.springframework.security.acls.model.PermissionGrantingStrategy;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//
//
//
//import net.sf.ehcache.CacheManager;
//import net.sf.ehcache.Ehcache;
//
//@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true,  securedEnabled = true)
//public class AclContext {
//	
//	
////	@Value("${spring.datasource.url}")
////    private String jdbcUrl;
////    @Value("${spring.datasource.username}")
////    private String username;
////    @Value("${spring.datasource.password}")
////    private String password;
////    @Value("${spring.datasource.driver-class-name}")
////    private String driverClass;
//
//	@Autowired
//	private DataSource dataSource;
//	
//	@Bean
//    public MethodSecurityExpressionHandler createExpressionHandler() throws PropertyVetoException{
//        DefaultMethodSecurityExpressionHandler securityExpressionHandler = new DefaultMethodSecurityExpressionHandler();
//        securityExpressionHandler.setPermissionEvaluator(new AclPermissionEvaluator(aclService()));
//        securityExpressionHandler.setPermissionCacheOptimizer(new AclPermissionCacheOptimizer(aclService()));
//        return securityExpressionHandler;
//    }
//
//
//	@Bean
//	public DefaultMethodSecurityExpressionHandler expressionHandler() throws PropertyVetoException{
//		DefaultMethodSecurityExpressionHandler dmseh = new DefaultMethodSecurityExpressionHandler();
//
//		dmseh.setPermissionEvaluator(permissionEvaluator());
//		dmseh.setPermissionCacheOptimizer(permissionCacheOptimizer());
//		return dmseh;
//	}
//
//	@Bean
//	public AclPermissionCacheOptimizer permissionCacheOptimizer() throws PropertyVetoException{
//		return new AclPermissionCacheOptimizer(aclService());
//	}
//
//	@Bean
//	public AclPermissionEvaluator permissionEvaluator() throws PropertyVetoException{
//		
////		return new AclPermissionEvaluator(aclService());
//		AclPermissionEvaluator pe = new AclPermissionEvaluator(aclService());
//		pe.setPermissionFactory(permissionFactory());
//		return pe;
//	}
//	
////	public DataSource dataSource() throws PropertyVetoException {
////        final ComboPooledDataSource cpds = new ComboPooledDataSource(true);
////        cpds.setJdbcUrl(jdbcUrl);
////        cpds.setUser(username);
////        cpds.setPassword(password);
////        cpds.setDriverClass(driverClass);
////        return cpds;
////    }
//
//	@Bean
//	public JdbcMutableAclService aclService() throws PropertyVetoException{
//		return new JdbcMutableAclService(dataSource, lookupStrategy(), aclCache());
//	}
//
//	@Bean
//	public LookupStrategy lookupStrategy() throws PropertyVetoException{
//		
////		return new BasicLookupStrategy(
////		           dataSource,
////		           aclCache(),
////		           aclAuthorizationStrategy(),
////		           consoleAuditLogger());
//		BasicLookupStrategy ls = new BasicLookupStrategy(dataSource, aclCache(), aclAuthorizationStrategy(),
//				consoleAuditLogger());
//
//		ls.setPermissionFactory(permissionFactory());
//		return ls;
//	}
//
//	@Bean
//	public ConsoleAuditLogger consoleAuditLogger() {
//		return new ConsoleAuditLogger();
//	}
//
//	@Bean
//	public AclAuthorizationStrategy aclAuthorizationStrategy() {
//		return new AclAuthorizationStrategyImpl(new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"));
//	}
//
//	@Bean
//	public EhCacheBasedAclCache aclCache() {
//		return new EhCacheBasedAclCache(ehcache(), permissionGrantingStrategy(), aclAuthorizationStrategy());
//	}
//
//	@Bean
//	public PermissionGrantingStrategy permissionGrantingStrategy() {
//		return new DefaultPermissionGrantingStrategy(consoleAuditLogger());
//	}
//
//	@Bean
//	public Ehcache ehcache() {
//		EhCacheFactoryBean cacheFactoryBean = new EhCacheFactoryBean();
//		cacheFactoryBean.setCacheManager(cacheManager());
//		cacheFactoryBean.setCacheName("aclCache");
//		cacheFactoryBean.setMaxBytesLocalHeap("1M");
//		cacheFactoryBean.setMaxEntriesLocalHeap(0L);
//		cacheFactoryBean.afterPropertiesSet();
//		return cacheFactoryBean.getObject();
//	}
//	
//
//	@Bean
//	public CacheManager cacheManager() {
//		EhCacheManagerFactoryBean cacheManager = new EhCacheManagerFactoryBean();
//		cacheManager.setAcceptExisting(true);
//		cacheManager.setCacheManagerName(CacheManager.getInstance().getName());
//		cacheManager.afterPropertiesSet();
//		return cacheManager.getObject();
//	}
//
//	@Bean
//	public DefaultPermissionFactory permissionFactory() {
//		return new DefaultPermissionFactory(CustomPermission.class);
//	}
//	
//	
//	
////	@Bean
////    public MethodSecurityExpressionHandler 
////      defaultMethodSecurityExpressionHandler() {
////        DefaultMethodSecurityExpressionHandler expressionHandler
////          = new DefaultMethodSecurityExpressionHandler();
////        AclPermissionEvaluator permissionEvaluator 
////          = new AclPermissionEvaluator(aclService());
////        expressionHandler.setPermissionEvaluator(permissionEvaluator);
////        return expressionHandler;
////    }
////    
////    @Bean 
////    public JdbcMutableAclService aclService() { 
////        return new JdbcMutableAclService(
////          dataSource, lookupStrategy(), aclCache()); 
////    }
////    
////    @Bean
////    public AclAuthorizationStrategy aclAuthorizationStrategy() {
//////        return new AclAuthorizationStrategyImpl(
//////          new SimpleGrantedAuthority("ROLE_ADMIN"));
////    	 return new AclAuthorizationStrategyImpl(
////                 new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"),
////                 new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"),
////                 new SimpleGrantedAuthority("ROLE_ADMINISTRATOR")
////         );
////    }
////     
////    @Bean
////    public PermissionGrantingStrategy permissionGrantingStrategy() {
////        return new DefaultPermissionGrantingStrategy(
////          new ConsoleAuditLogger());
////    }
////     
////    @Bean
////    public EhCacheBasedAclCache aclCache() {
////        return new EhCacheBasedAclCache(
////          aclEhCacheFactoryBean().getObject(), 
////          permissionGrantingStrategy(), 
////          aclAuthorizationStrategy()
////        );
////    }
////     
////    @Bean
////    public EhCacheFactoryBean aclEhCacheFactoryBean() {
////        EhCacheFactoryBean ehCacheFactoryBean = new EhCacheFactoryBean();
////        ehCacheFactoryBean.setCacheManager(aclCacheManager().getObject());
////        ehCacheFactoryBean.setCacheName("aclCache");
////        return ehCacheFactoryBean;
////    }
////     
////    @Bean
////    public EhCacheManagerFactoryBean aclCacheManager() {
////        return new EhCacheManagerFactoryBean();
////    }
////     
////    @Bean
////    public LookupStrategy lookupStrategy() { 
////        return new BasicLookupStrategy(
////          dataSource, 
////          aclCache(), 
////          aclAuthorizationStrategy(), 
////          new ConsoleAuditLogger()
////        ); 
////    }
//
//}
//
