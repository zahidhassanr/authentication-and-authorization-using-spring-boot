package com.grp.cokreates.security.auth.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class Oauth2ResourceServer extends
	    ResourceServerConfigurerAdapter {
	    @Override
	    public void configure(HttpSecurity http) throws Exception {
//	        http
//	            .authorizeRequests()
//	            .anyRequest().authenticated().and().httpBasic();
	    	
	    	 http.
	    	 cors().disable().
	        authorizeRequests().antMatchers("/sec/master/authentication/**", "/oauth/authorize").permitAll()
			.and()
			.authorizeRequests().antMatchers("/actuator**").permitAll()
			.anyRequest().authenticated(); 
	        
//	        http
//	        .authorizeRequests()
//	        .anyRequest().permitAll();
	    	
//	    	http
//			.antMatcher("/api/**")
//			.authorizeRequests()
//				.antMatchers("/api/**").access("#oauth2.hasScope('read_profile')");
//	    	 http
//	         .csrf().disable()
//	         .anonymous().disable()
//	         .httpBasic().disable()
//	         .cors().disable()
//	         .authorizeRequests()
//	             .anyRequest().authenticated();
	    }
	    
//	    @Bean
//	    @LoadBalanced
//	    RestTemplate restTemplate() {
//	        return new RestTemplate();
//	    }
	    
	   
	}

