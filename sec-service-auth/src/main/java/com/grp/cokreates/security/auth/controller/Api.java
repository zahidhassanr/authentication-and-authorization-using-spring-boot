//package com.grp.cokreates.security.auth.controller;
//
//import org.springframework.beans.factory.annotation.Value;
//
//
//public class Api {
//	
//	@Value("${hrm.service.url}")
//	private String hrmUrl;
//	
//	@Value("${cmn.service.url}")
//	private String cmnServiceUrl;
//	
//	@Value("${cmn.service.notification}")
//	private String cmnNotificationUrl;
//
//	public String hrm_master_info_get_by_oid = hrmUrl + "/employee-master-info/v1/get-by-oid";
//	public String hrm_employee_get_by_oid = hrmUrl + "/employee/v1/get-by-oid";
//	public String cmn_get_employee_by_oid_set = cmnServiceUrl + "/search/v1/get-list-by-oid-set";
//	public String cmn_service_get_name = cmnServiceUrl + "/search/v1/get-employees-by-name";
//	public String hrm_personal_info__get_by_oid = hrmUrl + "/employee-personal-info/v1/get-by-employee-oid";
//	//public String cmn_stakeholder_by_office = "http://192.168.178.93:8084/office/v1/get-stakeholder-by-office-oid";
//	public String email_api = cmnNotificationUrl + "/api/v1/updated-email";
//	public String get_admin_by_office = hrmUrl + "/employee/v1/get-admin-by-office-oid";
//	
//	public String DNS = "http://grp.gov.bd/";
//	public String ast_dashboard_url = "/web/ast/";
//	public String prc_dashboard_url = "/web/prc/";
//	public String mem_dashboard_url = "/web/mem/";
//	public String inv_dashboard_url = "/web/inv/";
//	public String hrm_dashboard_url = "/web/hrm/";
//	public String cmn_zuul_url = "/api/";
//	public String inv_zuul_url =  "/inv-api/";
//	
//
//}
