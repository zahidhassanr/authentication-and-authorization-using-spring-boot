package com.grp.cokreates.security.auth.controller;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.grp.cokreates.security.auth.entity.AccessToken;
import com.grp.cokreates.security.auth.service.AccessTokenService;
import com.grp.cokreates.security.auth.service.AuthWsClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sec/master/authentication/v1")
public class RefreshTokenController {

	@Autowired
	private AuthWsClient authWsClient;

	@Autowired
	private AccessTokenService accService;

	@CrossOrigin
	@RequestMapping(value = "/get-refresh-token", method = RequestMethod.POST)
	public ResponseEntity<String> getRefreshToken(@RequestBody JsonNode data) {
		log.info("URL /get-refresh-token");
		log.info("Request  {}", data);

		if (!data.has("refresh_token") || !data.has("clientId") || !data.has("clientPassword")) {
			return new ResponseEntity<String>("Invalid request", HttpStatus.BAD_REQUEST);
		}

		String clientId = data.get("clientId").asText();
		String clientPassword = data.get("clientPassword").asText();
		String refresh_token = data.get("refresh_token").asText();

		AccessToken token = accService.findByRefreshToken(refresh_token);

		if (token == null) {
			return new ResponseEntity<String>("Invalid Token", HttpStatus.BAD_REQUEST);

		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("grant_type", "refresh_token");
		map.put("refresh_token", refresh_token);

		/*
		 * 
		 * RestTemplate restTemplate = new RestTemplate();
		 * HttpEntity<MultiValueMap<String, String>> entity=new
		 * HttpEntity<>(map,createHeaders(clientId,clientPassword));
		 * log.info("HttpEntity {}",entity);
		 * 
		 */

		ResponseEntity<String> response = authWsClient.oauthRefreshToken(map, getToken(clientId, clientPassword));
		/*
		 * try { response=restTemplate.exchange(url, HttpMethod.POST, entity,
		 * String.class); }catch (Exception e) {
		 * 
		 * return new ResponseEntity<String>("Invalid credentials",
		 * HttpStatus.BAD_REQUEST);
		 * 
		 * }
		 */
		return response;

	}

	String getToken(String username, String password) {
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		return authHeader;
	}

	@SuppressWarnings("serial")
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);

				log.info("authHeader {}", authHeader);

				set("Authorization", authHeader);

				set("Content-Type", "application/x-www-form-urlencoded");
				set("Accept", "application/json");

			}
		};
	}

}
