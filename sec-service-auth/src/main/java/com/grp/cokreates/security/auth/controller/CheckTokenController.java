package com.grp.cokreates.security.auth.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.print.attribute.standard.DateTimeAtCompleted;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grp.cokreates.security.auth.entity.AccessToken;
import com.grp.cokreates.security.auth.entity.ResponseObject;
import com.grp.cokreates.security.auth.service.AccessTokenService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sec/master/authentication/v1")
public class CheckTokenController {

	@Autowired
	private AccessTokenService accTokenService;
	
	@CrossOrigin
	@RequestMapping(value = "/check/token", method = RequestMethod.POST)
	public ResponseObject angularToken(@RequestBody JsonNode data) throws IOException 
	{
		
		log.info("URL /check/token");
		log.info("Request  {}",data);
		
		if( !data.has("token") )
		{
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			log.info("Response  {}",response);
			return response;
		}
		
		String accessToken = data.get("token").asText();
		String claims = null;
		try {
			Jwt jwtToken = JwtHelper.decode(accessToken);    //Decode jwt token
			claims = jwtToken.getClaims();
		} catch(Exception e) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "cannot parse token", new ArrayList<>());
			log.info("Response  {}",response);
			return response;
		}
		
	    ObjectMapper objMapper = new ObjectMapper();
	    JsonNode nodeToken = objMapper.readTree(claims);

	    log.info("token expiry time   {} today {}" , nodeToken.get("exp").asInt() , System.currentTimeMillis()/1000);
		
	    long tokenExpired = nodeToken.get("exp").asInt();
	    long currentTime =  System.currentTimeMillis()/1000;
	    
	    if(currentTime > tokenExpired) {
	    	ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Token expired", "message: token expired");
			log.info("Response  {}",response);
			return response;	
	    }
	    
		AccessToken token = accTokenService.findByToken(accessToken);
		
		if(token == null)
		{
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid Token", new ArrayList<>());
			log.info("Response  {}",response);
			return response;
		}
		
		String message = "{ message : Token Found }";
		ResponseObject response = new ResponseObject(200, "No Error", message);
		log.info("Response  {}",response);
		return response;
	}
}
