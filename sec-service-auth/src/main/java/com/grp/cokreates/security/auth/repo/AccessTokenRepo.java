package com.grp.cokreates.security.auth.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.grp.cokreates.security.auth.entity.AccessToken;


public interface AccessTokenRepo extends JpaRepository<AccessToken, String>{
	
	

	@Query("select at from AccessToken at where at.token =:token")
    AccessToken findByToken(@Param(value="token") String token);

	@Query("select at from AccessToken at where at.refreshToken =:refreshToken")
    AccessToken findRefreshToken(@Param(value="refreshToken") String refreshToken);
	

//	@Query("select at from AccessToken at where at.authenticationId =:authenticationId")
//    AccessToken FindAuthenticationId(@Param(value="authenticationId") String authenticationId);
	
	@Modifying
	@Query("delete from AccessToken at where at.username =:userid")
	void deleteByUserId(@Param(value="userid") String userid);
	
	@Modifying
	@Query("delete from AccessToken at where at.token =:token")
	void deleteByToken(@Param(value="token") String token);

	@Query("select at from AccessToken at where at.username =:username")
    List<AccessToken> FindByUserName(@Param(value="username") String username);

}

