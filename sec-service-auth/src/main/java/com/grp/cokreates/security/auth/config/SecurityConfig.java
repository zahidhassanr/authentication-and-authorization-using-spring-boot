package com.grp.cokreates.security.auth.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.grp.cokreates.security.auth.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService users;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final String ROLE_PREFIX = "";
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(users).passwordEncoder(bCryptPasswordEncoder);        //using Bcrypt encoder to encode the user password
        
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
   //     super.configure(http);
        
//        http.
//        authorizeRequests().antMatchers("/sec/master/authentication/**", "/oauth/authorize").permitAll()
//		.and()
//		.authorizeRequests().antMatchers("/actuator*", "/**").permitAll()
//		.anyRequest().authenticated();          //Permit apis having /authorize/ and authorize all other
//    
        
        
//        
//        http
//        .csrf()
//        .disable()
//        .antMatcher("/**")
//        .authorizeRequests()
//        .antMatchers("/sec/master/authentication/**", "/oauth/**", "/actuator*")
//        .permitAll()
//        .anyRequest()
//        .authenticated();
        
     /*   
    	http.authorizeRequests()
        .anyRequest().fullyAuthenticated()
        .antMatchers(HttpMethod.GET, "/**").permitAll()
     .and()
        .httpBasic()
     .and()
        .csrf().disable();
        http
        .authorizeRequests()
        .antMatchers("/authorize/**").permitAll()
        .anyRequest().authenticated();
        
        
        
      
        
        http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/oauth/*").permitAll();
     */   
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
       // web.ignoring().antMatchers("/sec/master/authentication/**","/sec/master/office/**","/sec/master/employee/**", "/oauth/authorize", "/actuator/**");
    }
    
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
    	return super.authenticationManagerBean();
    }
    

    
    
    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(ROLE_PREFIX);
    }

}

