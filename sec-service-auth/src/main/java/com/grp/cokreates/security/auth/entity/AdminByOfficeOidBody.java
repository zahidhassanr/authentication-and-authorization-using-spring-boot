package com.grp.cokreates.security.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminByOfficeOidBody {

	private String officeOid;
}
