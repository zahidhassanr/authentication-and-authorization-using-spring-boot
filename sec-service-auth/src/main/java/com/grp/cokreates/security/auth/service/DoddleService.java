package com.grp.cokreates.security.auth.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.auth.entity.Doddle;
import com.grp.cokreates.security.auth.repo.DoddleRepository;

@Service
@Transactional
public class DoddleService {
	@Autowired
	private DoddleRepository doddleRepository;
	
	public Doddle findByOid(String oid) {
		return doddleRepository.findByOid(oid);
	}
	
	public List<Doddle> findAll() {
		return doddleRepository.findAll();
	}
	
	public Doddle update(Doddle doddle) {
		return doddleRepository.save(doddle);
	}
}
