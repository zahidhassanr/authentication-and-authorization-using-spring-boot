//package com.grp.cokreates.security.auth.service;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.grp.cokreates.security.auth.entity.OfficeModule;
//import com.grp.cokreates.security.auth.repo.OfficeModuleRepository;
//
//@Service
//public class OfficeModuleService {
//	
//	@Autowired
//	private OfficeModuleRepository officeRepo;
//	
//	
//	public String findByDashboard(String moduleid, String stakeholderOid)
//	{
//		OfficeModule om = officeRepo.findByModuleOidAndStakeholderOid(moduleid, stakeholderOid);
//		if(om == null)
//		{
//			return null;
//		}
//		else {
//			return om.getDashboardUrl();
//		}
//	}
//	
//	public String findByService(String moduleid, String stakeholderOid)
//	{
//		OfficeModule om = officeRepo.findByModuleOidAndStakeholderOid(moduleid, stakeholderOid);
//		if(om == null)
//		{
//			return null;
//		}
//		else {
//			return om.getServiceUrl();
//		}
//	}
//	
//	public List<OfficeModule> findByStakeholder(List<String> oids){
//		
//		return officeRepo.findByStakeholderOidIn(oids);
//	}
//	
//	public List<OfficeModule> getList(){
//		return officeRepo.findAll();
//	}
//	
//	public OfficeModule create(OfficeModule officeModule) {
//		return officeRepo.save(officeModule);
//	}
//	
//	public OfficeModule update(OfficeModule officeModule) {
//		return officeRepo.save(officeModule);
//	}
//	
//	public List<OfficeModule> findByStaekholderOid(String oid) {
//		return officeRepo.findByStakeholderOid(oid);
//	}
//	
//	public void delete(OfficeModule offModule) {
//		officeRepo.delete(offModule);
//	}
//
//}



package com.grp.cokreates.security.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.auth.entity.OfficeModule;
import com.grp.cokreates.security.auth.repo.OfficeModuleRepository;

@Service
public class OfficeModuleService {
	
	@Autowired
	private OfficeModuleRepository officeRepo;
	
	
	public String findByDashboard(String moduleid, String officeid)
	{
		OfficeModule om = officeRepo.findByModuleOidAndStakeholderOid(moduleid, officeid);
		if(om == null)
		{
			return null;
		}
		else {
			return om.getDashboardUrl();
		}
	}
	
	public String findByService(String moduleid, String officeid)
	{
		OfficeModule om = officeRepo.findByModuleOidAndStakeholderOid(moduleid, officeid);
		if(om == null)
		{
			return null;
		}
		else {
			return om.getServiceUrl();
		}
	}
	
	public List<OfficeModule> findByOffice(List<String> oids){
		
		return officeRepo.findByStakeholderOidIn(oids);
	}
	
	public List<OfficeModule> getList(){
		return officeRepo.findAll();
	}
	
	public OfficeModule create(OfficeModule officeModule) {
		return officeRepo.save(officeModule);
	}
	
	public OfficeModule update(OfficeModule officeModule) {
		return officeRepo.save(officeModule);
	}
	
	public void delete(OfficeModule offModule) {
		officeRepo.delete(offModule);
	}

}

