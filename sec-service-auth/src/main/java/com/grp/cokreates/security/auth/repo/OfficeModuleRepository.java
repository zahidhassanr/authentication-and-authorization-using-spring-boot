//package com.grp.cokreates.security.auth.repo;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.grp.cokreates.security.auth.entity.OfficeModule;
//
//public interface OfficeModuleRepository extends JpaRepository<OfficeModule, String>{
//
//	
//	OfficeModule findByModuleOidAndStakeholderOid(String moduleOid, String stakeholderOid);
//	
//	List<OfficeModule> findByStakeholderOidIn(List<String> oids);
//	
//	List<OfficeModule> findByStakeholderOid(String oid);
//}



package com.grp.cokreates.security.auth.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.cokreates.security.auth.entity.OfficeModule;

public interface OfficeModuleRepository extends JpaRepository<OfficeModule, String>{

	OfficeModule findByModuleOidAndStakeholderOid(String moduleOid, String stakeOid);
	
	List<OfficeModule> findByStakeholderOidIn(List<String> oids);
	
}
