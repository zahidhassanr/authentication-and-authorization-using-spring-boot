package com.grp.cokreates.security.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.grp.cokreates.security.auth.entity.AccessToken;
import com.grp.cokreates.security.auth.entity.ResponseObject;
import com.grp.cokreates.security.auth.service.AccessTokenService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sec/master/authentication/v1")
public class LogoutController {
	
	@Autowired
	private AccessTokenService accTokenService;
	
	@CrossOrigin
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseObject logout(@RequestBody JsonNode data)
	{
		log.info("URL /logout");
		log.info("Request {}", data);
		
		if( !data.has("token") )
		{
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		
		String accessToken= data.get("token").asText();
		
		AccessToken token = accTokenService.findByToken(accessToken);
		
		if(token == null)
		{
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid Token", "[]");
			return response;
		}
		
		
		
		accTokenService.deleteByToken(accessToken);
		
		String message = "{ message: Token is deleted }";
		
		ResponseObject response = new ResponseObject(200, "No Error", message);
	
		log.info("Response {}",response);
		
		return response;
	}

}
