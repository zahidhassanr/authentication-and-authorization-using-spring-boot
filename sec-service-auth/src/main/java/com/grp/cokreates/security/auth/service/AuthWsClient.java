package com.grp.cokreates.security.auth.service;

import java.util.Map;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.JsonNode;

import feign.Headers;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;

@FeignClient(name="auth-service" ,url="${auth.url}")
public interface AuthWsClient {
	
	@RequestMapping(method=RequestMethod.POST, value="/oauth/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@Headers("Content-Type: application/x-www-form-urlencoded")
	public ResponseEntity<String> oauthToken(@RequestBody Map<String, ?> request, @RequestHeader("Authorization") String token, @RequestHeader("officeOid") String officeOid, @RequestHeader("officeUnitOid") String officeUnitOid, @RequestHeader("officeUnitPostOid") String officeUnitPostOid, @RequestHeader("employeeOfficeOid") String employeeOfficeOid);
	
	
	@RequestMapping(method=RequestMethod.POST, value="/oauth/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@Headers("Content-Type: application/x-www-form-urlencoded")
	public ResponseEntity<String> oauthRefreshToken(@RequestBody Map<String, ?> request, @RequestHeader("Authorization") String token);
	
	
//	 @RequestMapping(method=RequestMethod.POST, value="/sec/master/authentication/v1/check/token", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
//	 public ResponseEntity<JsonNode>  checkToken(@RequestBody String dto);
	
	@Configuration
	class FeignSimpleEncoderConfig {
		
		@Bean
        Encoder feignFormEncoder(ObjectFactory<HttpMessageConverters> converters) {
            return new SpringFormEncoder(new SpringEncoder(converters));
        }
    
	}

}
