package com.grp.cokreates.security.auth.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ZuulUrlResponse {

	String zuul_url;
	List<String> officeOids;
}
