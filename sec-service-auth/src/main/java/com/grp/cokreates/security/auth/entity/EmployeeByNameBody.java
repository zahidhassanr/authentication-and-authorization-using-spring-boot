package com.grp.cokreates.security.auth.entity;

import java.io.Serializable;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeByNameBody implements Serializable{

	@NotNull
	private String[] listOfOfficeOid;
	
	@NotNull
    private String[] listOfOfficeUnitOid;
    
	@NotNull
	private String[] listOfOfficeUnitPostOid;
    
	@NotNull
	private String[] listOfPostOid;
	
	@NotNull
	private String name;
    
	@NotNull
	private int limit;
  
	@NotNull
	private int offset;
}
