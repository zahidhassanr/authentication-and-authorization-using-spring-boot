package com.grp.cokreates.security.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "doddle", schema = "sec")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Doddle {
	@Id	
	@Column(name="oid")
	private String oid;
	
	@Column(name="file_oid")
	private String fileOid;
}
