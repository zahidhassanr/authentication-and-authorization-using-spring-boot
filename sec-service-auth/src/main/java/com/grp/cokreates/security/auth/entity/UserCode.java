package com.grp.cokreates.security.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_code", schema = "sec")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCode {
	
	@Id
	@Column(name="oid")
	private String Oid;
	
	@Column(name="user_oid")
	private String userOid;
	
	@Column(name="code")
	private String code;
	
	@Column(name="type")
	private String type;
	
	@Column(name="expiration")
	private long expiration;
	
	@Column(name="email")
	private String email;
	
	@Column(name="phone")
	private String phone;
}
