package com.grp.cokreates.security.auth.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.cokreates.security.auth.entity.UserCode;

public interface UserCodeRepository extends JpaRepository<UserCode, String>{
	public UserCode findByUserOidAndCode(String userOid, String code);
	public List<UserCode> findAllByUserOidAndType(String userOid, String type);
}