package com.grp.cokreates.security.auth.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.grp.cokreates.security.auth.entity.EmailEmployeeBody;
import com.grp.cokreates.security.auth.entity.EmailRequestBody;
import com.grp.cokreates.security.auth.entity.EmployeeByFilter;
import com.grp.cokreates.security.auth.entity.EmployeeByNameBody;
import com.grp.cokreates.security.auth.entity.EmployeeByOid;
import com.grp.cokreates.security.auth.entity.EmployeeRequestByOidBody;
import com.grp.cokreates.security.auth.entity.EmployeeRequestHeader;
import com.grp.cokreates.security.auth.entity.OfficeModule;
import com.grp.cokreates.security.auth.entity.ResponseObject;
import com.grp.cokreates.security.auth.entity.SmsEmployeeBody;
import com.grp.cokreates.security.auth.entity.SmsRequestBody;
import com.grp.cokreates.security.auth.entity.StakeholderOffice;
import com.grp.cokreates.security.auth.entity.StakeholderOfficeRequest;
import com.grp.cokreates.security.auth.entity.UserCode;
import com.grp.cokreates.security.auth.entity.Users;
import com.grp.cokreates.security.auth.entity.ZuulUrlResponse;
import com.grp.cokreates.security.auth.service.OfficeModuleService;
import com.grp.cokreates.security.auth.service.UserCodeService;
import com.grp.cokreates.security.auth.service.UserService;

import javassist.expr.NewArray;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sec/master/user-management/v1")
public class EmployeeOfficeController {


	@Autowired
	private UserService userService;

	@Autowired
	private OfficeModuleService officeModuleService;
	
	@Autowired
	private UserCodeService uCodeService;
	
	@Value("${hrm.service.url}")
	private String hrmUrl;
	
	@Value("${cmn.service.url}")
	private String cmnServiceUrl;
	
	@Value("${cmn.service.notification}")
	private String cmnNotificationUrl;
	
/*
	
	@CrossOrigin
	@RequestMapping(value = "/get-by-office-oid", method = RequestMethod.POST)
	public ResponseObject getByOfficeOid(@RequestBody JsonNode data) {
		log.info("URl /get-by-office-oid");

		log.info("Request  {}", data.toString());

		String officeOid = data.get("oid").asText();

		List<Object> objects = new ArrayList<Object>();

		List<EmployeeOffice> employeeOfficeByoffice = employeeOfficeService.findByOffice(officeOid);

		log.info("employeeOfficeByoffice {}", employeeOfficeByoffice);

		for (EmployeeOffice em : employeeOfficeByoffice) {
			EmployeeMasterInfo emInfo = em.getEmployeeMasterInfo();

			log.info("emInfo {}", emInfo);

			Users user = userService.findByOid(emInfo.getGrpUsername());

			log.info("user {}", user);

			ObjectMapper object = new ObjectMapper();

			JsonNode node = object.valueToTree(emInfo);

			ObjectNode tokenObject = node.deepCopy();

			if (user == null) {
				tokenObject.put("userId", "null");
			} else {

				tokenObject.put("userId", user.getUsername());
			}

			objects.add(tokenObject);

		}

		ResponseObject response = new ResponseObject(200, "No Error", objects);

		log.info("Response  {}", response);

		return response;

	}

	@CrossOrigin
	@RequestMapping(value = "/get-by-office-unit-oid", method = RequestMethod.POST)
	public ResponseObject getByOfficeUnitOid(@RequestBody JsonNode data) {
		log.info("URl /get-by-office-unit-oid");

		log.info("Request  {}", data.toString());

		String officeUnitOid = data.get("oid").asText();

		List<Object> objects = new ArrayList<Object>();

		List<EmployeeOffice> employeeOfficeByofficeUnit = employeeOfficeService.findByOfficeUnit(officeUnitOid);

		log.info("employeeOfficeByofficeUnit {}", employeeOfficeByofficeUnit);

		for (EmployeeOffice em : employeeOfficeByofficeUnit) {
			EmployeeMasterInfo emInfo = em.getEmployeeMasterInfo();

			log.info("emInfo {}", emInfo);

			Users user = userService.findByOid(emInfo.getGrpUsername());

			log.info("user {}", user);

			ObjectMapper object = new ObjectMapper();

			JsonNode node = object.valueToTree(emInfo);

			ObjectNode tokenObject = node.deepCopy();

			if (user == null) {
				tokenObject.put("userId", "null");
			} else {

				tokenObject.put("userId", user.getUsername());
			}

			objects.add(tokenObject);

		}

		ResponseObject response = new ResponseObject(200, "No Error", objects);

		log.info("Response  {}", response);

		return response;

	}

	@CrossOrigin
	@RequestMapping(value = "/get-by-office-unit-post-oid", method = RequestMethod.POST)
	public ResponseObject getByOfficeUnitPostOid(@RequestBody JsonNode data) {
		log.info("URl /get-by-office-unit-post-oid");

		log.info("Request  {}", data.toString());

		String officeUnitPostOid = data.get("oid").asText();

		List<Object> objects = new ArrayList<Object>();

		List<EmployeeOffice> employeeOfficeByofficeUnitPost = employeeOfficeService
				.findByOfficeUnitPost(officeUnitPostOid);

		log.info("employeeOfficeByofficeUnit {}", employeeOfficeByofficeUnitPost);

		for (EmployeeOffice em : employeeOfficeByofficeUnitPost) {
			EmployeeMasterInfo emInfo = em.getEmployeeMasterInfo();

			log.info("emInfo {}", emInfo);

			Users user = userService.findByOid(emInfo.getGrpUsername());

			log.info("user {}", user);

			ObjectMapper object = new ObjectMapper();

			JsonNode node = object.valueToTree(emInfo);

			ObjectNode tokenObject = node.deepCopy();

			if (user == null) {
				tokenObject.put("userId", "null");
			} else {

				tokenObject.put("userId", user.getUsername());
			}

			objects.add(tokenObject);

		}

		ResponseObject response = new ResponseObject(200, "No Error", objects);

		log.info("Response  {}", response);

		return response;

	}


*/
	
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/get-by-filter", method = RequestMethod.POST)
	public ResponseObject getByFilter(@RequestBody EmployeeByNameBody data) {

		log.info("Request {}", data);
				
		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); 

		Map<String, String> meta = new HashMap<>();
		
		EmployeeByFilter employee = new EmployeeByFilter();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(data);
		
		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByFilter> entity = new HttpEntity<EmployeeByFilter>(employee, createHeaders());

		ResponseEntity<JsonNode> response = null;
		
		System.out.println(entity);

		try {
			response = restTemplate.exchange(cmnServiceUrl + "/search/v1/get-employees-by-name",
					HttpMethod.POST, entity, JsonNode.class);

		} catch (Exception e) {
			log.info("Not Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					new ArrayList<>());
			return finalResponse;
		}


		
		List<Object> objects = new ArrayList<Object>();

		ObjectMapper object = new ObjectMapper();

		JsonNode node = object.valueToTree(response);

		JsonNode body = node.get("body");

		JsonNode bodyData = body.get("body").get("data");

		if (bodyData.isArray()) {
			for (final JsonNode objNode : bodyData) {

				ObjectNode tokenObject = objNode.deepCopy();

				String employeeOid = tokenObject.get("oid").asText();

				Users userByEmployee = userService.findByEmployeeOid(employeeOid);
			

				if (userByEmployee == null) {
					tokenObject.put("userOid", "null");
					tokenObject.put("userId", "null");
				} else {

					tokenObject.put("userOid", userByEmployee.getOid());

					tokenObject.put("userId", userByEmployee.getUsername());
				}

				objects.add(tokenObject);

			}

		}


		ResponseObject finalResponse = new ResponseObject(200, "No Error", objects);

		log.info("Response  {}", finalResponse);
		
		return finalResponse;

	}




	@CrossOrigin
	@RequestMapping(value = "/get-by-filter-active", method = RequestMethod.POST)
	public ResponseObject getByFilterActive(@RequestBody EmployeeByNameBody data) {

		log.info("Request {}", data);
				
		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); 

		Map<String, String> meta = new HashMap<>();
		
		EmployeeByFilter employee = new EmployeeByFilter();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(data);
		
		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByFilter> entity = new HttpEntity<EmployeeByFilter>(employee, createHeaders());

		ResponseEntity<JsonNode> response = null;
		
		System.out.println(entity);

		try {
			response = restTemplate.exchange(cmnServiceUrl + "/search/v1/get-employees-by-name",
					HttpMethod.POST, entity, JsonNode.class);

		} catch (Exception e) {
			log.info("Not Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					new ArrayList<>());
			return finalResponse;
		}


		
		List<Object> objects = new ArrayList<Object>();

		ObjectMapper object = new ObjectMapper();

		JsonNode node = object.valueToTree(response);

		JsonNode body = node.get("body");

		JsonNode bodyData = body.get("body").get("data");

		if (bodyData.isArray()) {
			for (final JsonNode objNode : bodyData) {

				ObjectNode tokenObject = objNode.deepCopy();

				String employeeOid = tokenObject.get("oid").asText();

				Users userByEmployee = userService.findByEmployeeOid(employeeOid);
			

				if (userByEmployee == null) {
					tokenObject.put("userOid", "");
					tokenObject.put("userId", "");
					tokenObject.put("status", "");
				} else {

					tokenObject.put("userOid", userByEmployee.getOid());

					tokenObject.put("userId", userByEmployee.getUsername());
					
					tokenObject.put("status", userByEmployee.getStatus());
				}

				objects.add(tokenObject);

			}

		}


		ResponseObject finalResponse = new ResponseObject(200, "No Error", objects);

		log.info("Response  {}", finalResponse);
		
		return finalResponse;

	}


	@SuppressWarnings("serial")
	HttpHeaders createHeaders() {
		return new HttpHeaders() {
			{

				set("Content-Type", "application/json");

			}
		};
	}

	@CrossOrigin
	@RequestMapping(value = "/get-dashboard", method = RequestMethod.POST)
	public ResponseObject getDashboard(@RequestBody JsonNode data) {
		log.info("url /get-service");
		 
		if (!data.has("officeOid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}

		log.info("Request{}", data);

		String officeId = data.get("officeOid").asText();
		
		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
														// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

		Map<String, String> meta = new HashMap<>();
		
		List<String> oids = new ArrayList<>();
		
		oids.add(officeId);
		
		StakeholderOfficeRequest body = new StakeholderOfficeRequest(oids,"No");
		
		StakeholderOffice request = new StakeholderOffice();
		

		request.setHeader(header);
		request.setMeta(meta);
		request.setBody(body);
		
		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<StakeholderOffice> entity = new HttpEntity<StakeholderOffice>(request, createHeaders());

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(cmnServiceUrl + "/office/v1/get-stakeholder-by-office-oid",
					HttpMethod.POST, entity, JsonNode.class);

		} catch (Exception e) {
			log.info("Exception in api stakeholder_by_office");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find stakeholder",
					"message: Exception in api stakeholder_by_office");
			return finalResponse;
		}

		
		JsonNode data1 = response.getBody().get("body").get("data");
		
		if(data1.size() == 0) {
			log.info("No stakeholder Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find stakeholder",
					new ArrayList<>());
			return finalResponse;
		} else {
			data1 = response.getBody().get("body").get("data").get(0);
		}
		
		String stakeHolderOid = data1.get("stakeholder").get("oid").asText();		

		String MEM_DASHBOARD = officeModuleService.findByDashboard("11", stakeHolderOid);
		String AST_DASHBOARD = officeModuleService.findByDashboard("9", stakeHolderOid);
		String PRC_DASHBOARD = officeModuleService.findByDashboard("7", stakeHolderOid);
		String INV_DASHBOARD = officeModuleService.findByDashboard("8", stakeHolderOid);
		String PRJ_DASHBOARD = officeModuleService.findByDashboard("10", stakeHolderOid);
		String BUD_DASHBOARD = officeModuleService.findByDashboard("4", stakeHolderOid);
		String AUD_DASHBOARD = officeModuleService.findByDashboard("6", stakeHolderOid);
		String ACC_DASHBOARD = officeModuleService.findByDashboard("5", stakeHolderOid);
		String HRM_DASHBOARD = officeModuleService.findByDashboard("3", stakeHolderOid);
		String GED_DASHBOARD = officeModuleService.findByDashboard("12", stakeHolderOid);

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode map = mapper.createObjectNode();

		map.put("MEM_DASHBOARD", MEM_DASHBOARD);
		map.put("AST_DASHBOARD", AST_DASHBOARD);
		map.put("PRC_DASHBOARD", PRC_DASHBOARD);
		map.put("INV_DASHBOARD", INV_DASHBOARD);
		map.put("PRJ_DASHBOARD", PRJ_DASHBOARD);
		map.put("BUD_DASHBOARD", BUD_DASHBOARD);
		map.put("AUD_DASHBOARD", AUD_DASHBOARD);
		map.put("ACC_DASHBOARD", ACC_DASHBOARD);
		map.put("HRM_DASHBOARD", HRM_DASHBOARD);
		map.put("GED_DASHBOARD", GED_DASHBOARD);

		ResponseObject finalResponse = new ResponseObject(200, "No Error", map);

		log.info("Response  {}", finalResponse);

		return finalResponse;
	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/get-service", method = RequestMethod.POST)
	public ResponseObject getService(@RequestBody JsonNode data) {

		log.info("url /get-service");
		if (!data.has("officeOid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}

		log.info("Request{}", data);

		String officeId = data.get("officeOid").asText();
		
		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
														// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

		Map<String, String> meta = new HashMap<>();
		
		List<String> oids = new ArrayList<>();
		
		oids.add(officeId);
		
		StakeholderOfficeRequest body = new StakeholderOfficeRequest(oids,"No");
		
		StakeholderOffice request = new StakeholderOffice();
		

		request.setHeader(header);
		request.setMeta(meta);
		request.setBody(body);
		
		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<StakeholderOffice> entity = new HttpEntity<StakeholderOffice>(request, createHeaders());

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(cmnServiceUrl + "/office/v1/get-stakeholder-by-office-oid",
					HttpMethod.POST, entity, JsonNode.class);

		} catch (Exception e) {
			log.info("Exception in api stakeholder_by_office");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find stakeholder",
					"message: Exception in api stakeholder_by_office");
			return finalResponse;
		}

		
		JsonNode data1 = response.getBody().get("body").get("data");
		
		if(data1.size() == 0) {
			log.info("No stakeholder Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find stakeholder",
					new ArrayList<>());
			return finalResponse;
		} else {
			data1 = response.getBody().get("body").get("data").get(0);
		}
		
		String stakeHolderOid = data1.get("stakeholder").get("oid").asText();	

		String MEM_DASHBOARD = officeModuleService.findByService("11", stakeHolderOid);
		String AST_DASHBOARD = officeModuleService.findByService("9", stakeHolderOid);
		String PRC_DASHBOARD = officeModuleService.findByService("7", stakeHolderOid);
		String INV_DASHBOARD = officeModuleService.findByService("8", stakeHolderOid);
		String PRJ_DASHBOARD = officeModuleService.findByService("10", stakeHolderOid);
		String BUD_DASHBOARD = officeModuleService.findByService("4", stakeHolderOid);
		String AUD_DASHBOARD = officeModuleService.findByService("6", stakeHolderOid);
		String ACC_DASHBOARD = officeModuleService.findByService("5", stakeHolderOid);
		String HRM_DASHBOARD = officeModuleService.findByService("3", stakeHolderOid);
		String GED_DASHBOARD = officeModuleService.findByService("12", stakeHolderOid);

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode map = mapper.createObjectNode();

		map.put("MEM_SERVICE", MEM_DASHBOARD);
		map.put("AST_SERVICE", AST_DASHBOARD);
		map.put("PRC_SERVICE", PRC_DASHBOARD);
		map.put("INV_SERVICE", INV_DASHBOARD);
		map.put("PRJ_SERVICE", PRJ_DASHBOARD);
		map.put("BUD_SERVICE", BUD_DASHBOARD);
		map.put("AUD_SERVICE", AUD_DASHBOARD);
		map.put("ACC_SERVICE", ACC_DASHBOARD);
		map.put("HRM_SERVICE", HRM_DASHBOARD);
		map.put("GED_SERVICE", GED_DASHBOARD);

		ResponseObject finalResponse = new ResponseObject(200, "No Error", map);

		log.info("Response  {}", finalResponse);

		return finalResponse;
	}

	@CrossOrigin
	@RequestMapping(value = "/get-user", method = RequestMethod.POST)
	public ResponseObject getUser(@RequestBody JsonNode data) {

		if (!data.has("userId")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		String userOid = data.get("userId").asText();

		Users user = userService.findByOid(userOid);

		if (user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "No user", "[]");
			return response;
		}

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode map = mapper.createObjectNode();

		map.put("userId", user.getUsername());
		map.put("active", user.getStatus());

		ResponseObject finalResponse = new ResponseObject(200, "No Error", map);

		log.info("Response  {}", finalResponse);

		return finalResponse;

	}

	@CrossOrigin
	@RequestMapping(value = "/get-users-by-employees", method = RequestMethod.POST)
	public ResponseObject getUserByEmployees(@RequestBody String[] data) {

		List<String> list = new ArrayList<String>();

		for (String oid : data) {

			list.add(oid);
		}

		List<Users> allUser = userService.findByEmployeeOids(list);

		List<Object> objects = new ArrayList<Object>();

		for (Users user : allUser) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("employeeOid", user.getEmployeeOid());
			map.put("userOid", user.getOid());

			objects.add(map);

		}
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", objects);
		log.info("Response {}", response);
		return response;

	}

	@CrossOrigin
	@RequestMapping(value = "/get-employee-by-user", method = RequestMethod.POST)
	public ResponseObject getEmployeeByUser(@RequestBody JsonNode data) {

		if (!data.has("userOid")) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");

			return response;

		}

		String userOid = data.get("userOid").asText();

		Users user = userService.findByOid(userOid);

		if (user == null) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid credential", "[]");

			return response;

		}

		String employeeId = user.getEmployeeOid();

		//EmployeeMasterInfo employee = emInfoService.findByOid(employeeId);
		
		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
														// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

		Map<String, String> meta = new HashMap<>();

		EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(employeeId);

		EmployeeByOid employee = new EmployeeByOid();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(body);

		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee, createHeaders());

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(hrmUrl + "/employee-master-info/v1/get-by-oid",
					HttpMethod.POST, entity, JsonNode.class);

		} catch (Exception e) {
			log.info("Not Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					"[]");
			return finalResponse;
		}

		System.out.println(response);
		
		JsonNode data1 = response.getBody().get("body").get("data");
		JsonNode em = null;
		
		for(JsonNode dat : data1) {
			em = dat;
		}

		ResponseObject finalresponse = new ResponseObject(HttpStatus.OK.value(), "No error", em);

		log.info("Response {}", finalresponse);


		return finalresponse;

	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/get-employeelist-by-userlist", method = RequestMethod.POST)
	public ResponseObject getUserListByEmployeeList(@RequestBody String[] data) {

		List<String> list = new ArrayList<String>();

		for (String oid : data) {

			list.add(oid);
		}

		List<Users> allUser = userService.findByOids(list);

		List<Object> objects = new ArrayList<Object>();

		for (Users user : allUser) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("employeeOid", user.getEmployeeOid());
			map.put("userOid", user.getOid());

			objects.add(map);

		}
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", objects);
		log.info("Response {}", response);
		return response;

	}

	
	@CrossOrigin
	@RequestMapping(value = "/get-zuul_url-by-office", method = RequestMethod.POST)
	public ResponseObject getZuulByOffice(@RequestBody String[] data) {
		
		List<String> list = new ArrayList<String>();

		for (String oid : data) {

			list.add(oid);
		}
		
		List<OfficeModule> officeModules = officeModuleService.findByOffice(list);
		
		Map<String, Set<String>> map =new HashMap<>();
		
		for(OfficeModule officeModule : officeModules ) {
			map.put(officeModule.getZuulUrl(), new HashSet<String>());
		}
		
		for(OfficeModule officeModule : officeModules ) {
			map.get(officeModule.getZuulUrl()).add(officeModule.getStakeholderOid());
		}
		
		
		
		List<Object> objects = new ArrayList<Object>();
		
		Iterator mapIterator = map.entrySet().iterator(); 
		
		while (mapIterator.hasNext()) { 
            Map.Entry mapElement = (Map.Entry)mapIterator.next(); 
            Set<String> set = (Set<String>) mapElement.getValue(); 
            

			
			List<String> lists = new ArrayList<String>();
			
			for(String s : set) {
				lists.add(s);
			}
            ZuulUrlResponse zuulRes = new ZuulUrlResponse();
            
            zuulRes.setZuul_url(mapElement.getKey().toString());
            zuulRes.setOfficeOids(lists);
			
			objects.add(zuulRes);
        } 
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", objects);
		log.info("Response {}", response);
		return response;
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/get-office-module-list", method = RequestMethod.POST)
	public ResponseObject getOfficeModuleList() {
		List<OfficeModule> getList = officeModuleService.getList();
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", getList);
		log.info("Response {}", response);
		return response;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/create-office-module", method = RequestMethod.POST)
	public ResponseObject createOfficeModule(@RequestBody OfficeModule officeModule) {
	
		OfficeModule newOfficeModule = officeModuleService.create(officeModule);
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", newOfficeModule);
		log.info("Response {}", response);
		return response;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/update-office-module", method = RequestMethod.POST)
	public ResponseObject updateOfficeModule(@RequestBody OfficeModule officeModule) {
	
		OfficeModule newOfficeModule = officeModuleService.create(officeModule);
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", newOfficeModule);
		log.info("Response {}", response);
		return response;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/delete-office-module", method = RequestMethod.POST)
	public ResponseObject deleteOfficeModule(@RequestBody OfficeModule officeModule) {
	
		officeModuleService.delete(officeModule);
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", "message: OfficeModule deleted");
		log.info("Response {}", response);
		return response;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/get-email", method = RequestMethod.POST)
	public ResponseObject getEmail(@RequestBody JsonNode data) {
		log.info("url /get-email");
		if (!data.has("userOid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		String userOid = data.get("userOid").asText();
		Users user = userService.findByOid(userOid);
		if(user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userOid", "[]");
			return response;
		}
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode map = mapper.createObjectNode();

		map.put("email", user.getEmail());
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", map);
		log.info("Response {}", response);

		return response;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/get-phone", method = RequestMethod.POST)
	public ResponseObject getPhone(@RequestBody JsonNode data) {
		log.info("url /get-phone");
		if (!data.has("userOid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		String userOid = data.get("userOid").asText();
		Users user = userService.findByOid(userOid);
		if(user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userOid", "[]");
			return response;
		}
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode map = mapper.createObjectNode();

		map.put("phone", user.getPhone());
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", map);
		log.info("Response {}", response);

		return response;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/email-code-send", method = RequestMethod.POST)
	public ResponseObject emailCode(@RequestBody JsonNode data) {
		log.info("url /email-code-send");
		if (!data.has("userOid") && !data.has("email") && !data.has("officeOid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		String userOid = data.get("userOid").asText();
		String email = data.get("email").asText();
		String officeOid = data.get("officeOid").asText();
		
		log.info("userOid {} officeOid {} email {}", userOid, officeOid, email);
		
		Users user = userService.findByOid(userOid);
		
		if(user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userId", new ArrayList<>());
			return response;
		} else {
			Users userByEmail = userService.findByEmail(email);
			if(userByEmail != null ) {
				ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "ইমেইলটি আগে ব্যবহার হয়েছে", new ArrayList<>());
				return response;
			}
			Random rn=new Random();
			String code=String.valueOf(rn.nextInt(10000000));
			String message = "<h4>জনাব/জনাবা,</h4>"  + "<p>আপনার ইমেইল ভেরিফাইড করতে নিচের OTP ব্যবহার করুন </p>" +"<h2>"+"OTP : "+ code +"</h2>";
			
			List<EmailEmployeeBody> listEm = new ArrayList<>();
			EmailEmployeeBody emBody = new EmailEmployeeBody(user.getEmployeeOid(), officeOid, email, "TO");
			listEm.add(emBody);
			EmailRequestBody emailBody = new EmailRequestBody(UUID.randomUUID().toString(), "EMAIL", "CMN", listEm, new ArrayList<>(), "Request for email verification", message, "NOW", "False");
			
			RestTemplate restTemplate1 = new RestTemplate();

			HttpEntity<EmailRequestBody> entity1 = new HttpEntity<EmailRequestBody>(emailBody, createHeaders());

			ResponseEntity<JsonNode> response1 = null;

			try {
				response1 = restTemplate1.exchange(cmnNotificationUrl + "/api/v1/updated-email",
						HttpMethod.POST, entity1, JsonNode.class);

			} catch (Exception e) {
				log.info("Exception in notification api {}", e);
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot send email",
						"message: Exception in notification api");
				return finalResponse;
			}

			UserCode newCode = new UserCode(UUID.randomUUID().toString(), userOid, code, "email", System.currentTimeMillis() + 600000, email, null);
			uCodeService.save(newCode);
			ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "Code Send", new ArrayList<>());
			return response;
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/email-code-check", method = RequestMethod.POST)
	public ResponseObject emailCodeCheck(@RequestBody JsonNode data) {
		log.info("url /email-code-check");
		if (!data.has("userOid") && !data.has("code")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", new ArrayList<>());
			return response;
		}
		
		String userOid = data.get("userOid").asText();
		String code = data.get("code").asText();
		UserCode uCode = uCodeService.findByOidAndCode(userOid, code);
		
		if(uCode == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid code", new ArrayList<>());
			return response;
		} else if(uCode.getExpiration() < System.currentTimeMillis()) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid code. Code expired", new ArrayList<>());
			uCodeService.remove(uCode);
			return response;
		} else {
			Users user = userService.findByOid(userOid);
			if(user == null) {
				ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userId", new ArrayList<>());
				return response;
			} else {
				user.setEmail(uCode.getEmail());
				userService.create(user);
				uCodeService.remove(uCode);
				ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "Email added", new ArrayList<>());
				return response;
			}
		}
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/sms-code-send", method = RequestMethod.POST)
	public ResponseObject smsCode(@RequestBody JsonNode data) {
		log.info("url /sms-code-send");
		if (!data.has("userOid") && !data.has("phone") && !data.has("officeOid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		String userOid = data.get("userOid").asText();
		String phone = data.get("phone").asText();
		String officeOid = data.get("officeOid").asText();
		
		log.info("userOid {} officeOid {} phone {}", userOid, officeOid, phone);
		
		Users user = userService.findByOid(userOid);
		
		if(user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userId", new ArrayList<>());
			return response;
		} else {
			Users userByPhone = userService.findByPhone(phone);
			if(userByPhone != null ) {
				ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "মোবাইল নম্বরটি আগে ব্যবহার হয়েছে", new ArrayList<>());
				return response;
			}
			Random rn=new Random();
			String code=String.valueOf(rn.nextInt(10000000));
			String message = "জনাব/জনাবা,"  + "আপনার মোবাইল নম্বর ভেরিফাইড করতে নিচের OTP ব্যবহার করুন " +"OTP : "+ code;
			
			List<SmsEmployeeBody> listEm = new ArrayList<>();
			SmsEmployeeBody emBody = new SmsEmployeeBody(user.getEmployeeOid(), officeOid, phone);
			listEm.add(emBody);
			SmsRequestBody smsBody = new SmsRequestBody(UUID.randomUUID().toString(), "EMAIL", "CMN", listEm, new ArrayList<>(), message, "NOW");
			
			RestTemplate restTemplate1 = new RestTemplate();

			HttpEntity<SmsRequestBody> entity1 = new HttpEntity<SmsRequestBody>(smsBody, createHeaders());

			ResponseEntity<JsonNode> response1 = null;

			try {
				response1 = restTemplate1.exchange(cmnNotificationUrl + "/api/v1/sms",
						HttpMethod.POST, entity1, JsonNode.class);

			} catch (Exception e) {
				log.info("Exception in notification api {}", e);
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot send email",
						"message: Exception in notification api");
				return finalResponse;
			}

			UserCode newCode = new UserCode(UUID.randomUUID().toString(), userOid, code, "phone", System.currentTimeMillis() + 600000, null, phone);
			uCodeService.save(newCode);
			ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "Code Send", new ArrayList<>());
			return response;
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/sms-code-check", method = RequestMethod.POST)
	public ResponseObject smsCodeCheck(@RequestBody JsonNode data) {
		log.info("url /sms-code-check");
		if (!data.has("userOid") && !data.has("code")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", new ArrayList<>());
			return response;
		}
		
		String userOid = data.get("userOid").asText();
		String code = data.get("code").asText();
		UserCode uCode = uCodeService.findByOidAndCode(userOid, code);
		
		if(uCode == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid code", new ArrayList<>());
			return response;
		} else if(uCode.getExpiration() < System.currentTimeMillis()) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid code. Code expired", new ArrayList<>());
			uCodeService.remove(uCode);
			return response;
		} else {
			Users user = userService.findByOid(userOid);
			if(user == null) {
				ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userId", new ArrayList<>());
				return response;
			} else {
				user.setPhone(uCode.getPhone());
				userService.create(user);
				uCodeService.remove(uCode);
				ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "Phone added", new ArrayList<>());
				return response;
			}
		}
		
	}

}

