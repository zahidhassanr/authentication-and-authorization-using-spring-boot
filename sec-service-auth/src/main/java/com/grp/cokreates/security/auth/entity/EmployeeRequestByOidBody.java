package com.grp.cokreates.security.auth.entity;

import java.io.Serializable;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class EmployeeRequestByOidBody implements Serializable{
	
	@NotNull
	private String oid;

}
