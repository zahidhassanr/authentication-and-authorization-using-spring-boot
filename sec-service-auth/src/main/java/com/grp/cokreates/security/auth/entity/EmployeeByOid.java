package com.grp.cokreates.security.auth.entity;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeByOid implements Serializable{
	
	private EmployeeRequestHeader header;
	
	private Map<String, String> meta;
	
	private EmployeeRequestByOidBody body;
	

}
