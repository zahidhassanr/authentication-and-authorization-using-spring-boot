package com.grp.cokreates.security.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "user_generatedtoken", schema = "sec")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserGeneratedString {
	
	@Id
	@Column(name="oid")
	private String Oid;
	
	@Column(name="user_oid")
	private String userOid;
	
	@Column(name="generated_string")
	private String generatedString;

}
