package com.grp.cokreates.security.auth.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsRequestBody {
	private String oid;	
	private String token;
	private String tag;
	private List<SmsEmployeeBody> listOfEmployees;
	private List<String> listOfGuests;
	private String message;
	private String scheduledTime;
}