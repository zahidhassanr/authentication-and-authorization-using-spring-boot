package com.grp.cokreates.security.auth.controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.grp.cokreates.security.auth.entity.AdminByOfficeOidBody;
import com.grp.cokreates.security.auth.entity.AdminByOfficeRequest;
import com.grp.cokreates.security.auth.entity.Doddle;
import com.grp.cokreates.security.auth.entity.EmailEmployeeBody;
import com.grp.cokreates.security.auth.entity.EmailRequestBody;
import com.grp.cokreates.security.auth.entity.EmployeeByOid;
import com.grp.cokreates.security.auth.entity.EmployeeByOidSet;
import com.grp.cokreates.security.auth.entity.EmployeeRequestByOidBody;
import com.grp.cokreates.security.auth.entity.EmployeeRequestHeader;
import com.grp.cokreates.security.auth.entity.ResponseObject;
import com.grp.cokreates.security.auth.entity.StakeholderOfficeRequest;
import com.grp.cokreates.security.auth.entity.UserGeneratedString;
import com.grp.cokreates.security.auth.entity.Users;
import com.grp.cokreates.security.auth.service.AuthWsClient;
import com.grp.cokreates.security.auth.service.DoddleService;
import com.grp.cokreates.security.auth.service.UserGeneratedService;
import com.grp.cokreates.security.auth.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sec/master/authentication/v1")
public class LoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private DoddleService doddleService;

	@Autowired
	private AuthWsClient authWsClient;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private UserGeneratedService userGeneratedService;

	@Value("${hrm.service.url}")
	private String hrmUrl;

	@Value("${cmn.service.url}")
	private String cmnServiceUrl;

	@Value("${cmn.service.notification}")
	private String cmnNotificationUrl;

	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseObject getToken(@RequestBody JsonNode userDetails) throws IOException {
		log.info("URL /login");
		if (!userDetails.has("userId") || !userDetails.has("userPassword") || !userDetails.has("grantType")
				|| !userDetails.has("clientId") || !userDetails.has("clientPassword")) {
			ResponseObject finalResponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request",
					"message:Invalid request");
			log.info("Response {}", finalResponse);

			return finalResponse;
		}

		String userId = userDetails.get("userId").asText();
		String userPassword = userDetails.get("userPassword").asText();
		String grantType = userDetails.get("grantType").asText();
		String clientId = userDetails.get("clientId").asText();
		String clientPassword = userDetails.get("clientPassword").asText();

		Users user = userService.find(userId);

		if (user == null || user.getStatus().matches("Inactive")) {
			log.info("userid not found");

			user = userService.findByEmail(userId);

			if (user == null || user.getStatus().matches("Inactive")) {
				log.info("email not found");

				user = userService.findByPhone(userId);

				if (user == null || user.getStatus().matches("Inactive")) {
					ResponseObject finalResponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(),
							"Invalid credentials", "message:Invalid userId or inactive users");
					log.info("Response {}", finalResponse);

					return finalResponse;
				} else {
					userId = user.getUsername();
				}
			} else {
				userId = user.getUsername();
			}
		}

		if (!encoder.matches(userPassword, user.getPassword())) {
			ResponseObject finalResponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid password",
					"message:Invalid password");
			log.info("Response {}", finalResponse);

			return finalResponse;
		}

		if (user.getEmployeeOid() == null) {
			ResponseObject finalResponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid credentials",
					"message: No employeeId for User");
			log.info("Response {}", finalResponse);

			return finalResponse;
		}

		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("grant_type", grantType);
		map1.put("username", userId);
		map1.put("password", userPassword);

		ResponseEntity<String> dummyresponse = null;

		try {
			dummyresponse = authWsClient.oauthToken(map1, getToken(clientId, clientPassword), "dummy", "dummy", "dummy",
					"dummy");
		} catch (Exception e) {
			ResponseObject finalResponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid credentials",
					"message:Invalid credentials");
			log.info("Response {}", finalResponse);

			return finalResponse;
		}

		ObjectMapper mapper1 = new ObjectMapper();
		JsonNode token1 = mapper1.readTree(dummyresponse.getBody());

		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
														// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

		Map<String, String> meta = new HashMap<>();

		List<String> oids = new ArrayList<>();
		oids.add(user.getEmployeeOid());

		// EmployeeRequestByOidBody body = new
		// EmployeeRequestByOidBody(user.getEmployeeOid());

		StakeholderOfficeRequest body = new StakeholderOfficeRequest(oids, "No");
		// EmployeeByOid employee = new EmployeeByOid();
		EmployeeByOidSet employee = new EmployeeByOidSet();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(body);

		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByOidSet> entity = new HttpEntity<EmployeeByOidSet>(employee,
				dummyEmployeeHeaders(token1.get("access_token").asText()));

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(cmnServiceUrl + "/search/v1/get-list-by-oid-set", HttpMethod.POST, entity,
					JsonNode.class);

		} catch (Exception e) {
			log.info("Exception in cmn api get_by_oid_set");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					"message: Exception in cmn api get_by_oid_set");
			return finalResponse;
		}

		JsonNode data = response.getBody().get("body").get("data");
		if (data.size() == 0) {
			log.info("Empty Employee");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "No employee",
					new ArrayList<>());
			return finalResponse;
		}

		List<Object> objects = new ArrayList<Object>();

		for (JsonNode node : data) {

			String officeOid = node.get("officeOid").asText();
			String officeUnitOid = node.get("officeUnitOid").asText();
			String officeUnitPostOid = node.get("officeUnitPostOid").asText();
			String employeeOfficeOid = node.get("employeeOfficeOid").asText();
			String officeNameBn = node.get("officeNameBn").asText();
			String officeUnitNameBn = node.get("officeUnitNameBn").asText();
			String officeUnitPostNameBn = node.get("officeUnitPostNameBn").asText();

			Map<String, String> map = new HashMap<String, String>();
			map.put("grant_type", grantType);
			map.put("username", userId);
			map.put("password", userPassword);

			ResponseEntity<String> response1 = null;

			try {
				response1 = authWsClient.oauthToken(map, getToken(clientId, clientPassword), officeOid, officeUnitOid,
						officeUnitPostOid, employeeOfficeOid);
				Users newUser = new Users(user.getOid(), user.getUsername(), user.getEmail(), user.getPassword(),
						user.getEmployeeOid(), user.getStatus(), user.getAccountExpired(), user.getCredentialsExpired(),
						user.getAccountLocked(), user.getTotalLoginAttempt(), 0, user.getPhone());
				userService.create(newUser);

			} catch (Exception e) {
				ResponseObject finalResponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid credentials",
						"message:Invalid credentials for feign call");
				log.info("Response {}", finalResponse);

				return finalResponse;

			}

			ObjectMapper mapper = new ObjectMapper();
			JsonNode token = mapper.readTree(response1.getBody());

			ObjectNode objectMap = mapper.createObjectNode();

			objectMap.put("oid", UUID.randomUUID().toString());
			objectMap.put("officeOid", officeOid);
			objectMap.put("officeUnitOid", officeUnitOid);
			objectMap.put("officeUnitPostOid", officeUnitPostOid);
			objectMap.put("officeName", officeNameBn);
			objectMap.put("officeUnitName", officeUnitNameBn);
			objectMap.put("officeunitPostName", officeUnitPostNameBn);
			objectMap.put("access_token", token.get("access_token"));

			objects.add(objectMap);
		}

		ResponseObject finalResponse = new ResponseObject(HttpStatus.OK.value(), "No error", objects);
		log.info("Response {}", finalResponse);

		return finalResponse;

	}

	String getToken(String username, String password) {
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		return authHeader;
	}

	@SuppressWarnings("serial")
	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
				set("Content-Type", "application/x-www-form-urlencoded");
				set("Accept", "application/json");
			}
		};
	}

	@CrossOrigin
	@RequestMapping(value = "/get-employee-by-user", method = RequestMethod.POST)
	public ResponseObject getEmployeeByUser(@RequestBody JsonNode data) {

		if (!data.has("userOid")) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");

			return response;

		}

		String userOid = data.get("userOid").asText();

		Users user = userService.findByOid(userOid);

		if (user == null) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid credential", "[]");

			return response;

		}

		String employeeId = user.getEmployeeOid();

		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
														// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

		Map<String, String> meta = new HashMap<>();

		EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(employeeId);

		EmployeeByOid employee = new EmployeeByOid();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(body);

		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee, createEmployeeHeaders());

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(hrmUrl + "/employee-master-info/v1/get-by-oid", HttpMethod.POST, entity,
					JsonNode.class);

		} catch (Exception e) {
			log.info("Not Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					new ArrayList<>());
			return finalResponse;
		}

		JsonNode data1 = response.getBody().get("body").get("data");
		JsonNode em = null;

		for (JsonNode dat : data1) {
			em = dat;
		}

		ResponseObject finalresponse = new ResponseObject(HttpStatus.OK.value(), "No error", em);

		log.info("Response {}", finalresponse);

		return finalresponse;

	}

	@CrossOrigin
	@RequestMapping(value = "/get-users-by-employees", method = RequestMethod.POST)
	public ResponseObject getUserByEmployees(@RequestBody String[] data) {

		List<String> list = new ArrayList<String>();

		for (String oid : data) {

			list.add(oid);
		}

		List<Users> allUser = userService.findByEmployeeOids(list);

		List<Object> objects = new ArrayList<Object>();

		for (Users user : allUser) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("employeeOid", user.getEmployeeOid());
			map.put("userOid", user.getOid());

			objects.add(map);

		}
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", objects);
		log.info("Response {}", response);
		return response;
	}

	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/get-employee-oid", method = RequestMethod.POST)
	 * public ResponseObject getEmployee(@RequestBody JsonNode userDetails) {
	 * log.info("url /get-employee");
	 * 
	 * log.info("Request  {}", userDetails);
	 * 
	 * String employeeId = userDetails.get("employeeId").asText();
	 * 
	 * EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid",
	 * "portal", "portal", "grp", "random", new Date(), "v1", 30, 3, 10, "abc"); //
	 * new EmployeeRequestHeader("random-uuid", "portal", "portal", // "grp",
	 * "random", "2019-01-06", "v1", 30, 3, 10, "ab");
	 * 
	 * Map<String, String> meta = new HashMap<>();
	 * 
	 * EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(employeeId);
	 * 
	 * EmployeeByOid employee = new EmployeeByOid();
	 * 
	 * employee.setHeader(header); employee.setMeta(meta); employee.setBody(body);
	 * 
	 * RestTemplate restTemplate = new RestTemplate();
	 * 
	 * HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee,
	 * createEmployeeHeaders());
	 * 
	 * ResponseEntity<JsonNode> response = null;
	 * 
	 * try { response = restTemplate.exchange(, HttpMethod.POST, entity,
	 * JsonNode.class);
	 * 
	 * } catch (Exception e) { log.info("Not Found"); ResponseObject finalResponse =
	 * new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
	 * "[]"); return finalResponse; }
	 * 
	 * System.out.println(response);
	 * 
	 * JsonNode data = response.getBody().get("body").get("data");
	 * 
	 * if (data.isArray()) { for (final JsonNode objNode : data) {
	 * System.out.println(objNode.get("officeOid").asText()); } }
	 * 
	 * return null; }
	 * 
	 */
	@SuppressWarnings("serial")
	HttpHeaders dummyEmployeeHeaders(String token) {
		return new HttpHeaders() {
			{

				set("Content-Type", "application/json");
				set("Authorization", "Bearer " + token);

			}
		};
	}

	@SuppressWarnings("serial")
	HttpHeaders createEmployeeHeaders() {
		return new HttpHeaders() {
			{

				set("Content-Type", "application/json");

			}
		};
	}

	@CrossOrigin
	@RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
	public ResponseObject forgotPass(@RequestBody JsonNode data) {
		log.info("url /forgot-password");
		if (!data.has("userId") && !data.has("url")) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request",
					new ArrayList<>());

			return response;

		}

		String userId = data.get("userId").asText();
		String url = data.get("url").asText();

		Users user = userService.find(userId);

		if (user == null) {
			user = userService.findByEmail(userId);
			if (user == null) {
				ResponseObject response = new ResponseObject(HttpStatus.NOT_FOUND.value(), "Invalid userId",
						new ArrayList<>());
				return response;
			}
		}

		String employeeId = user.getEmployeeOid();

		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc");

		Map<String, String> meta = new HashMap<>();

		EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(employeeId);

		EmployeeByOid employee = new EmployeeByOid();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(body);

		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee, createEmployeeHeaders());

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(hrmUrl + "/employee/v1/get-by-oid", HttpMethod.POST, entity,
					JsonNode.class);

		} catch (Exception e) {
			log.info("Exception in hrm api get_by_oid");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					"message: Exception in hrm api get_by_oid");
			return finalResponse;
		}

		JsonNode data1 = response.getBody().get("body").get("data");

		if (data1.size() == 0) {
			log.info("Empty data");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "No employee",
					new ArrayList<>());
			return finalResponse;
		}

		UserGeneratedString newGeneratedString = new UserGeneratedString(UUID.randomUUID().toString(), user.getOid(),
				UUID.randomUUID().toString());
		userGeneratedService.create(newGeneratedString);

		String officeOid = data1.get(0).get("officeOid").asText();
		String emailAddress = user.getEmail();

		log.info("employeeOid {} officeOid {} emailAddress {}", user.getEmployeeOid(), officeOid, emailAddress);

		String message = "<h4>জনাব/জনাবা,</h4>"
				+ "<p>আপনার পাসওয়ার্ড পুনরুদ্ধার এর জন্য নিচের লিঙ্কটিতে ক্লিক করুন। </p>" + "<a href=\"" + url
				+ newGeneratedString.getGeneratedString() + "\">" + url + newGeneratedString.getGeneratedString()
				+ "</a>" + "<div align=\"right\" style=\"margin-top:15px\">"
				+ "<img src=\"https://res.cloudinary.com/dgrxlkqfx/image/upload/v1576755107/logo_white-01_vrmgk5.svg\" style=\"width:104px;height:142px;\">"
				+ "</div>";

		List<EmailEmployeeBody> listEm = new ArrayList<>();
		EmailEmployeeBody emBody = new EmailEmployeeBody(user.getEmployeeOid(), officeOid, emailAddress, "TO");
		listEm.add(emBody);
		EmailRequestBody emailBody = new EmailRequestBody(UUID.randomUUID().toString(), "EMAIL", "CMN", listEm,
				new ArrayList<>(), "Request for password reset", message, "NOW", "False");

		RestTemplate restTemplate1 = new RestTemplate();

		HttpEntity<EmailRequestBody> entity1 = new HttpEntity<EmailRequestBody>(emailBody, createEmployeeHeaders());

		ResponseEntity<JsonNode> response1 = null;

		try {
			response1 = restTemplate1.exchange(cmnNotificationUrl + "/api/v1/updated-email", HttpMethod.POST, entity1,
					JsonNode.class);

		} catch (Exception e) {
			log.info("Exception in notification api");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot send email",
					"message: Exception in notification api");
			return finalResponse;
		}

		EmployeeRequestHeader adminHeader = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp",
				"random", new Date(), "v1", 30, 3, 10, "abc");

		Map<String, String> adminMeta = new HashMap<>();

		AdminByOfficeOidBody adminBody = new AdminByOfficeOidBody(officeOid);

		AdminByOfficeRequest admin = new AdminByOfficeRequest();

		admin.setHeader(adminHeader);
		admin.setMeta(adminMeta);
		admin.setBody(adminBody);

		HttpEntity<AdminByOfficeRequest> adminEntity = new HttpEntity<AdminByOfficeRequest>(admin,
				createEmployeeHeaders());

		ResponseEntity<JsonNode> adminResponse = null;

		try {
			adminResponse = restTemplate.exchange(hrmUrl + "/employee/v1/get-admin-by-office-oid", HttpMethod.POST,
					adminEntity, JsonNode.class);

		} catch (Exception e) {
			log.info("Exception in hrm api get_by_oid");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					"message: Exception in hrm api get_by_oid");
			return finalResponse;
		}

		JsonNode adminData = adminResponse.getBody().get("body").get("data");

		List<Object> objects = new ArrayList<>();

		for (JsonNode admins : adminData) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("nameBn", admins.get("nameBn").asText());
			map.put("phone", admins.get("phone").asText());

			objects.add(map);
		}

		ResponseObject finalresponse = new ResponseObject(HttpStatus.OK.value(), "email sent", objects);

		return finalresponse;

	}

	@CrossOrigin
	@RequestMapping(value = "/change-password", method = RequestMethod.POST)
	public ResponseObject resetPassword(@RequestBody JsonNode userDetails) {
		log.info("url /change-password");

		log.info("Request  {}", userDetails);

		if (!userDetails.has("userId") || !userDetails.has("newPassword") || !userDetails.has("confirmPassword")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}

		String genString = userDetails.get("userId").asText();
		UserGeneratedString uGen = userGeneratedService.findByGeneratedString(genString);

		if (uGen == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid ID",
					new ArrayList<>());
			return response;
		}
		String userId = uGen.getUserOid();
		String newPassword = userDetails.get("newPassword").asText();

		Users user = userService.findByOid(userId);

		if (user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid userId",
					new ArrayList<>());
			return response;
		} else if (newPassword.matches(user.getUsername())) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "password cannot be userId",
					new ArrayList<>());
			return response;
		} else {

			EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
					new Date(), "v1", 30, 3, 10, "abc");

			Map<String, String> meta = new HashMap<>();

			EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(user.getEmployeeOid());

			EmployeeByOid employee = new EmployeeByOid();

			employee.setHeader(header);
			employee.setMeta(meta);
			employee.setBody(body);

			RestTemplate restTemplate = new RestTemplate();

			HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee, createEmployeeHeaders());

			ResponseEntity<JsonNode> response = null;

			try {
				response = restTemplate.exchange(hrmUrl + "/employee-personal-info/v1/get-by-employee-oid",
						HttpMethod.POST, entity, JsonNode.class);

			} catch (Exception e) {
				log.info("Exception in hrm");
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(),
						"employee-personal-info service threw exception", new ArrayList<>());
				return finalResponse;
			}

			JsonNode data = response.getBody().get("body").get("data");
			if (data.size() == 0) {
				log.info("Not Found");
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "No employee",
						new ArrayList<>());
				return finalResponse;
			}

			log.info("emailAddress {}", data.get(0).get("emailAddress").asText());
			String emailAddress = data.get(0).get("emailAddress").asText();

			log.info("mobileNo {}", data.get(0).get("mobileNo").asText());
			String mobileNo = data.get(0).get("mobileNo").asText();

			log.info("phoneNo {}", data.get(0).get("mobileNo").asText());
			String phoneNo = data.get(0).get("phoneNo").asText();

			log.info("name {}", data.get(0).get("nameEn").asText());
			String NameEn = data.get(0).get("nameEn").asText();

			String[] splitted = NameEn.split(" ");

			for (String s : splitted) {
				if (s.length() > 2) {

					if (newPassword.contains(emailAddress) || newPassword.contains(phoneNo)
							|| newPassword.contains(mobileNo) || newPassword.contains(s)) {
						log.info("matched with some information");
						ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(),
								"password cannot contain email address or phone number or name", new ArrayList<>());
						return finalResponse;
					}
				}
			}

			Users newUser = new Users(user.getOid(), user.getUsername(), user.getEmail(), encoder.encode(newPassword),
					user.getEmployeeOid(), user.getStatus(), user.getAccountExpired(), user.getCredentialsExpired(),
					user.getAccountLocked(), user.getTotalLoginAttempt(), user.getLoginAttemptCount(), user.getPhone());

			userService.create(newUser);
		}

		userGeneratedService.deleteByGeneratedString(genString);
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", "message: password Modified");
		return response;

	}

	@CrossOrigin
	@RequestMapping(value = "/update-doodle", method = RequestMethod.POST)
	public ResponseObject updateDoddle(@RequestBody Doddle data) {
		log.info("url /update-doddle");

		log.info("Request  {}", data);

		Doddle temp = doddleService.findByOid(data.getOid());

		if (temp == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "No row in Database",
					new ArrayList<>());
			return response;
		} else {
			Doddle doddle = doddleService.update(data);
			ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", doddle);
			return response;
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/get-doodle", method = RequestMethod.POST)
	public ResponseObject getDoddle(@RequestBody JsonNode data) {
		log.info("url /get-doddle");

		log.info("Request  {}", data);

		if (!data.has("oid")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request",
					new ArrayList<>());
			return response;
		}

		String oid = data.get("oid").asText();

		Doddle doddle = doddleService.findByOid(oid);

		if (doddle == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid oid",
					new ArrayList<>());
			return response;
		} else {
			ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", doddle);
			return response;
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/get-doodle-list", method = RequestMethod.POST)
	public ResponseObject getDoddleList(@RequestBody JsonNode data) {
		log.info("url /get-doddle-list");
		List<Doddle> doddles = doddleService.findAll();
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", doddles);
		return response;
	}

}
