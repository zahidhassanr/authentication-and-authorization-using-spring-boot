package com.grp.cokreates.security.auth.service;


import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grp.cokreates.security.auth.entity.EmployeeByFilter;
import com.grp.cokreates.security.auth.entity.ResponseObject;
import com.grp.cokreates.security.auth.entity.Users;
import com.grp.cokreates.security.auth.repo.UserRepository;
import com.grp.cokreates.security.auth.repo.UserResourceDetails;

@Service
@Transactional
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	
	@Override
	public UserDetails loadUserByUsername(String uname) throws UsernameNotFoundException {
		
//		
//		RestTemplate restTemplate = new RestTemplate();
//		
//		String user = "{\n" + 
//				"	\"body\": {\n" + 
//				"		\"username\": \"" + uname + "\"\n" + 
//				"	}\n" + 
//				"}";
//
//		HttpEntity<String> entity = new HttpEntity<String>(user, createHeaders());
//
//		ResponseEntity<JsonNode> response = null;
//		
//		System.out.println(entity);
//
//		try {
//			response = restTemplate.exchange("http://13.71.28.250/sec/get/v1/credential",
//					HttpMethod.POST, entity, JsonNode.class);
//
//		} catch (Exception e) {
//			System.out.println("error " + e);
//		}
//		
//		System.out.println("response " + response);
//
//		JsonNode bodyData = response.getBody().get("body");
//		
//		System.out.println("body " + bodyData);
//		
//		System.out.println("name " + bodyData.get("username").asText() + bodyData.get("password").asText());
//		
//		Users newUser = new Users();
//		newUser.setUsername(bodyData.get("username").asText());
//		newUser.setPassword(bodyData.get("password").asText());
		Users user = userRepository.findByUsername(uname);
		return new UserResourceDetails(user);
	}
	
//	public List<Post> findByUserid(String userid)
//	{
//
//		User user= userRepository.findByUserid(userid);
//		
//		return user.getPosts();
//
//	}
	
	
//	@Transactional(rollbackFor = Exception.class)
	public Users create(Users user) {
		return userRepository.save(user);
		
	
	}
	
	public Users find(String userid)
	{
		return userRepository.findByUsername(userid);
	}
	
	public List<Users> findAll()
	{
		return (List<Users>) userRepository.findAll();
	}
	
	public Users findByEmail(String email)
	{
		return userRepository.findByEmail(email);
	}
	
	public Users findByOid(String oid)
	{
		return userRepository.findByOid(oid);
	}
	
	public List<Users> findByString(String userid)
	{
		return userRepository.findByString(userid);
	}
	
	public Users findByEmployeeOid(String oid)
	{
		return userRepository.findByEmployeeOid(oid);
	}
	
	public List<Users> findByEmployeeOids(List<String> oids){
		return userRepository.findByEmployeeOidIn(oids);
	}
	
	public List<Users> findByOids(List<String> oids){
		return userRepository.findByOidIn(oids);
	}
	
	public Users findByPhone(String phone) {
		return userRepository.findByPhone(phone);
	}
	
	HttpHeaders createHeaders() {
		return new HttpHeaders() {
			{

				set("Content-Type", "application/json");

			}
		};
	}
}
