package com.grp.cokreates.security.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "office_module", schema = "sec")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OfficeModule {

	@Id	
	@Column(name="oid")
	private String oid;
	
	@Column(name="module_oid")
	private String moduleOid;

	@Column(name="stakeholder_oid")
	private String stakeholderOid;
	
	@Column(name="dashboard_url")
	private String dashboardUrl;
	
	@Column(name="service_url")
	private String serviceUrl;
	
	@Column(name="zuul_url")
	private String zuulUrl;
}

//
//package com.grp.cokreates.security.auth.entity;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import lombok.Data;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//@Entity
//@Table(name = "office_module", schema = "sec")
//@Data
//@Getter
//@Setter
//@NoArgsConstructor
//public class OfficeModule {
//
//	@Id	
//	@Column(name="oid")
//	private String oid;
//	
//	@Column(name="module_oid")
//	private String moduleOid;
//	
//	@Column(name="office_oid")
//	private String officeOid;
//	
//	@Column(name="dashboard_url")
//	private String dashboardUrl;
//	
//	@Column(name="service_url")
//	private String serviceUrl;
//	
//	@Column(name="zuul_url")
//	private String zuulUrl;
//}
//
