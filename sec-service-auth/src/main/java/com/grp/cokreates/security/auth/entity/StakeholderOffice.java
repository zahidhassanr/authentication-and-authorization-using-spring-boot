package com.grp.cokreates.security.auth.entity;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StakeholderOffice  implements Serializable{

	private EmployeeRequestHeader header;
	
	private Map<String, String> meta;
	
	private StakeholderOfficeRequest body;
}
