package com.grp.cokreates.security.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.auth.entity.UserCode;
import com.grp.cokreates.security.auth.repo.UserCodeRepository;

@Service
public class UserCodeService {
	@Autowired
	private UserCodeRepository userCodeRepo;
	
	public void save(UserCode uCode) {
		List<UserCode> list = userCodeRepo.findAllByUserOidAndType(uCode.getUserOid(), uCode.getType());
		userCodeRepo.deleteAll(list);
		userCodeRepo.save(uCode);
	}
	
	public UserCode findByOidAndCode(String userOid, String code) {
		return userCodeRepo.findByUserOidAndCode(userOid, code);
	}
	
	public void remove(UserCode uCode) {
		userCodeRepo.delete(uCode);
	}

}
