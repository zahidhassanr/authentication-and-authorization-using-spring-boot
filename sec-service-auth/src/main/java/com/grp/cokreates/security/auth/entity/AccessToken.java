package com.grp.cokreates.security.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.oauth2.provider.OAuth2Authentication;



@Entity
@Table(name = "access_token", schema = "sec")
public class AccessToken {
	
	@Id
	@Column(name="ac_id")
    private String ac_id;
	
	@Column(name="token_id")
    private String tokenId;
	
	@Column(name="token")
	private String token;
	
	@Column(name="authentication_id")
    private String authenticationId;
	
	@Column(name="username")
    private String username;
	
	@Column(name="client_id")
    private String clientId;
	
	@Column(name="authentication")
    private String authentication;
	
	@Column(name="refresh_token")
    private String refreshToken;

	public String getId() {
		return ac_id;
	}

	public void setId(String id) {
		this.ac_id = id;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	
	public OAuth2Authentication getAuthentication() {
        return SerializableObjectConverter.deserialize(authentication);
    }
 
    public void setAuthentication(OAuth2Authentication authentication) {
        this.authentication = SerializableObjectConverter.serialize(authentication);
    }

	public AccessToken(String id, String tokenId, String token, String authenticationId, String username,
			String clientId, OAuth2Authentication authentication, String refreshToken) {
		super();
		this.ac_id = id;
		this.tokenId = tokenId;
		this.token = token;
		this.authenticationId = authenticationId;
		this.username = username;
		this.clientId = clientId;
		this.authentication = SerializableObjectConverter.serialize(authentication);
		this.refreshToken = refreshToken;
	}
	
	
	public AccessToken()
	{
		
	}

}

