package com.grp.cokreates.security.auth.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StakeholderOfficeRequest {
	private List<String> oids;
	private String strict;
}
