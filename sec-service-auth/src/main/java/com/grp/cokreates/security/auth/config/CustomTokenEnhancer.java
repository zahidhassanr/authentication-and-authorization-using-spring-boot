package com.grp.cokreates.security.auth.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.grp.cokreates.security.auth.entity.Office;
import com.grp.cokreates.security.auth.entity.Users;
import com.grp.cokreates.security.auth.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomTokenEnhancer implements TokenEnhancer {
	

	@Autowired
	Office office;
	
	@Autowired
	private UserService userService;
		
	
	
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		
		
		log.info("Office oid "+ office.getOfficeOid());
		log.info("OfficeUnit oid "+ office.getOfficeUnitOid());
		log.info("OfficeUnitPost oid "+ office.getOfficeUnitPostOid());
		log.info("employeeOfficeOid "+ office.getEmployeeOfficeOid());
		
		Map<String, Object> additional = new HashMap<>();
		
		String userId = authentication.getName();
		
		Users user = userService.find(userId);
		
		String userOid = user.getOid();
		
		additional.put("userOid", userOid);
		
		
		
		
		/*
		
		Users user = userService.find(userId);
		
		EmployeeOffice employee = user.getEmployeeOffice();
		
		additional.put("employee_id", employee.getOid());
		additional.put("office_id", employee.getOfficeOid());
		additional.put("stakeholder_id", employee.getStakeholder().getOid());
		additional.put("office_unit_id", employee.getOfficeUnitOid());
		
		List<OfficeUnitPosts> officeUnitPost = employee.getOfficeUnitposts();
		
		List<String> roles = new ArrayList<>();
		
		for( OfficeUnitPosts offUnpost : officeUnitPost)
		{
			roles.add(offUnpost.getNameEn());
		}
		
		additional.put("roles", roles);
		
		
		UserClient ucl = userClientService.findByPostId(userId, clientId); // fetch posts using useer_id and client_id

		
		List<Post> posts=ucl.getPosts();
		List<String> roles = new ArrayList<>();

		
		for (Post post : posts) {
			
			System.out.println(post.getId());
			
			roles.add(post.getPostname());
		}
		
		
		additional.put("roles", roles);
		
		
		String selectSql = "SELECT oid from hrm.employee_master_info WHERE postgres_username = ?";
		Object[] params = { userId};
		 
		   
        // define SQL types of the arguments
        int[] types = {Types.VARCHAR};
		
 
        String employee_oid = jdbcTemplate.queryForObject(selectSql,params,String.class);
		
		
		System.out.println(employee_oid);
		
		additional.put("employeeId", employee_oid);
		
		String updateSql = "SELECT office_id from hrm.employee_office where  oid = ? ";
		Object[] params1 = { employee_oid};
		 
		   
        // define SQL types of the arguments
        
		
 
		String office_id = jdbcTemplate.queryForObject(updateSql, params1, String.class);
		
		System.out.println(office_id);
		
		additional.put("office_id",office_id);
		*/
		
		
		//EmployeeMasterInfo master = employeeMasterService.findByusername(userOid);
		
		String employeeId = user.getEmployeeOid();
		
		additional.put("employeeId", employeeId);
		
	//	EmployeeOffice employeeOffice = employeeOfficeService.findByEMployee(employeeId);
		
		String officeOid = office.getOfficeOid();
		String officeUnitOid = office.getOfficeUnitOid();
		String officeUnitPostOid = office.getOfficeUnitPostOid();
		String employeeOfficeOid = office.getEmployeeOfficeOid();
		
		additional.put("officeId", officeOid);
		additional.put("officeUnitId", officeUnitOid);
		additional.put("officeUnitPostId", officeUnitPostOid);
		additional.put("employeeOfficeId", employeeOfficeOid);
		
		DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) accessToken;
		token.setAdditionalInformation(additional);
		
		
		return accessToken;
	}
	

	

}