package com.grp.cokreates.security.auth.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.auth.entity.UserGeneratedString;
import com.grp.cokreates.security.auth.repo.UserGeneratedRepository;

@Service
@Transactional
public class UserGeneratedService {

	@Autowired
	private UserGeneratedRepository userGenRepo;
	
	public void create(UserGeneratedString u)
	{
		userGenRepo.save(u);
	}
	
	public void delete(UserGeneratedString u)
	{
		userGenRepo.delete(u);;
	}
	
	public void deleteByGeneratedString(String generated)
	{
		userGenRepo.deleteByGeneratedString(generated);
	}
	
	public UserGeneratedString findByGeneratedString(String generated)
	{
		return userGenRepo.findByGeneratedString(generated);
	}
}
