package com.grp.cokreates.security.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Office {

	private String officeOid;
	private String officeUnitOid;
	private String officeUnitPostOid;
	private String employeeOfficeOid;
	
	
}
