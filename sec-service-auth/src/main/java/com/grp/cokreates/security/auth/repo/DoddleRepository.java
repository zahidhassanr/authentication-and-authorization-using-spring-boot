package com.grp.cokreates.security.auth.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grp.cokreates.security.auth.entity.Doddle;


public interface DoddleRepository extends JpaRepository<Doddle, String>{

	public Doddle findByOid(String oid);
}
