package com.grp.cokreates.security.auth.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.grp.cokreates.security.auth.entity.EmployeeByOid;
import com.grp.cokreates.security.auth.entity.EmployeeRequestByOidBody;
import com.grp.cokreates.security.auth.entity.EmployeeRequestHeader;
import com.grp.cokreates.security.auth.entity.ResponseObject;
import com.grp.cokreates.security.auth.entity.Users;
import com.grp.cokreates.security.auth.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sec/master/user-management/v1")
public class SignupControler {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder encoder;
	

	@Value("${hrm.service.url}")
	private String hrmUrl;
	
	@Value("${cmn.service.url}")
	private String cmnServiceUrl;
	
	@Value("${cmn.service.notification}")
	private String cmnNotificationUrl;

	@CrossOrigin
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseObject signup(@RequestBody JsonNode userDetails, @RequestHeader("Authorization") String authorization) {
		log.info("url /signup");

		log.info("Request  {}", userDetails);

		if (!userDetails.has("userId") || !userDetails.has("userPassword") || !userDetails.has("employeeId")
				|| !userDetails.has("active")) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}

		String userId = userDetails.get("userId").asText();
		String userPassword = userDetails.get("userPassword").asText();
		String employeeId = userDetails.get("employeeId").asText();
		String active = userDetails.get("active").asText();

		if (active.matches("true")) {
			active = "Active";
		} else {
			active = "Inactive";
		}

		log.info("userId {}  password {} employeeId {}", userId, userPassword, employeeId);

		Users userByEmployee = userService.findByEmployeeOid(employeeId);

		if (userByEmployee != null) {
			// Users existUser = userService.findByOid(employee.getGrpUsername());

			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("userId", userByEmployee.getUsername());

			ResponseObject response = new ResponseObject(HttpStatus.FORBIDDEN.value(), "exist", map);
			log.info("Response {}", response);

			return response;
		}

		EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
				new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
														// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

		Map<String, String> meta = new HashMap<>();

		EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(employeeId);

		EmployeeByOid employee = new EmployeeByOid();

		employee.setHeader(header);
		employee.setMeta(meta);
		employee.setBody(body);

		RestTemplate restTemplate = new RestTemplate();

		HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee, createEmployeeHeaders(authorization));

		ResponseEntity<JsonNode> response = null;

		try {
			response = restTemplate.exchange(
					hrmUrl + "/employee-master-info/v1/get-by-oid", HttpMethod.POST,
					entity, JsonNode.class);

		} catch (Exception e) {
			log.info("Not Found");
			ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
					"[]");
			return finalResponse;
		}

		log.info("HRM response {}", response);

		JsonNode data = response.getBody().get("body").get("data");
		
		//System.out.println(data);

		if (data.size() == 0) {
			ResponseObject finalresponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "No employee found", new ArrayList<>());
			log.info("Response {}", finalresponse);
			return finalresponse;
		}

		Users user = userService.find(userId);

		log.info("User found");

		if (user == null) {
			
			Users newUser = new Users(UUID.randomUUID().toString(), userId, userId, encoder.encode(userPassword),
					employeeId, active, "No", "No", "No",3, 0, "");

			userService.create(newUser);

			log.info("Employee created");

			ResponseObject finalresponse = new ResponseObject(HttpStatus.OK.value(), "No error",
					"message: user created");
			log.info("Response {}", finalresponse);

			return finalresponse;

		} else {
			ResponseObject finalresponse = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "UserId exist", "[]");
			return finalresponse;
		}

	}

	@SuppressWarnings("serial")
	HttpHeaders createHeaders() {
		return new HttpHeaders() {
			{

				set("Content-Type", "application/json");

			}
		};
	}

	@SuppressWarnings("serial")
	HttpHeaders createEmployeeHeaders(String authorization) {
		return new HttpHeaders() {
			{

				set("Content-Type", "application/json");
				set("Authorization", authorization);

			}
		};
	}
	

	
	
	@CrossOrigin
	@RequestMapping(value = "/search-employee", method = RequestMethod.POST)
	public ResponseObject getEmployee(@RequestBody JsonNode userDetails) {
		log.info("url /search-employee");

		log.info("Request  {}", userDetails);

		String employeeId = userDetails.get("employeeId").asText();

		Users userByEmployee = userService.findByEmployeeOid(employeeId);

		if (userByEmployee != null) {
			// Users existUser = userService.findByOid(employee.getGrpUsername());

			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("userId", userByEmployee.getUsername());

			ResponseObject response = new ResponseObject(HttpStatus.FORBIDDEN.value(), "exist", map);
			log.info("Response {}", response);

			return response;

		} else {
			
			EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
					new Date(), "v1", 30, 3, 10, "abc"); // new EmployeeRequestHeader("random-uuid", "portal", "portal",
															// "grp", "random", "2019-01-06", "v1", 30, 3, 10, "ab");

			Map<String, String> meta = new HashMap<>();

			EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(employeeId);

			EmployeeByOid employee = new EmployeeByOid();

			employee.setHeader(header);
			employee.setMeta(meta);
			employee.setBody(body);

			RestTemplate restTemplate = new RestTemplate();

			HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee, createHeaders());

			ResponseEntity<JsonNode> response = null;

			try {
				response = restTemplate.exchange(
						hrmUrl + "/employee-personal-info/v1/get-by-employee-oid", HttpMethod.POST,
						entity, JsonNode.class);

			} catch (Exception e) {
				log.info("Exception in api");
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "cannot find employee",
						"message: Exception in hrm personal info api");
				return finalResponse;
			}

			log.info("HRM response {}", response);
			
			

			JsonNode data = response.getBody().get("body").get("data");
			
			if (data.size() == 0) {
				ResponseObject finalresponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "No employee found", new ArrayList<>());
				log.info("Response {}", finalresponse);
				return finalresponse;
			}
			data = response.getBody().get("body").get("data").get(0);
			
			String emailAdress = data.get("emailAddress").asText();
			String phoneNUmber = data.get("mobileNo").asText();
			
			if(phoneNUmber.contains("+88")) {
				phoneNUmber.replace("+88", "");
			}
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode map = mapper.createObjectNode();

			map.put("emailAdress", emailAdress);
			map.put("phone", phoneNUmber);
																																																																						
			ResponseObject finalResponse = new ResponseObject(HttpStatus.OK.value(), "No user", map);
			log.info("Response {}", finalResponse);

			return finalResponse;
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/search-userid", method = RequestMethod.POST)
	public ResponseObject getCode(@RequestBody JsonNode userDetails) {
		log.info("Request {}", userDetails);

		String userid = userDetails.get("userId").asText();

		List<Users> users = userService.findByString(userid);

		int maxCode = 0000;

		if (users.isEmpty()) {

			String Stringmaxcode = Integer.toString(maxCode + 1);

			if (Stringmaxcode.length() == 1) {
				Stringmaxcode = "000" + Stringmaxcode;
			} else if (Stringmaxcode.length() == 2) {
				Stringmaxcode = "00" + Stringmaxcode;
			} else if (Stringmaxcode.length() == 3) {
				Stringmaxcode = "0" + Stringmaxcode;
			}

			ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", "code: " + Stringmaxcode);
			log.info("Response {}", response);

			return response;
		}

		for (Users user : users) {
			String username = user.getUsername();

			String Stringcode = username.replace(userid, "");

			System.out.println("code " + Stringcode);

			int code;
			try {
				code = Integer.parseInt(Stringcode);
			} catch (Exception e) {
				continue;
			}
			if (code > maxCode) {
				maxCode = code;
			}

		}

		String Stringmaxcode = Integer.toString(maxCode + 1);

		if (Stringmaxcode.length() == 1) {
			Stringmaxcode = "000" + Stringmaxcode;
		} else if (Stringmaxcode.length() == 2) {
			Stringmaxcode = "00" + Stringmaxcode;
		} else if (Stringmaxcode.length() == 3) {
			Stringmaxcode = "0" + Stringmaxcode;
		}

		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", "code: " + Stringmaxcode);
		log.info("Response {}", response);

		return response;

	}

	@CrossOrigin
	@RequestMapping(value = "/modify-user", method = RequestMethod.POST)
	public ResponseObject modify(@RequestBody JsonNode userDetails) {
		log.info("url /modify-user");

		log.info("Request  {}", userDetails);

		if (!userDetails.has("userId") || !userDetails.has("userPassword") || !userDetails.has("employeeId")
				|| !userDetails.has("active")) {

			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}

		String userId = userDetails.get("userId").asText();
		String userPassword = userDetails.get("userPassword").asText();
		String employeeId = userDetails.get("employeeId").asText();
		String active = userDetails.get("active").asText();

		if (active.matches("true")) {
			active = "Active";
		} else {
			active = "Inactive";
		}

		log.info("userId {}  password {} employeeId {}", userId, userPassword, employeeId);

		Users user = userService.find(userId);

		log.info("User found");

		if (userPassword.isEmpty()) {

			log.info("Password unmodified");
			Users newUser = new Users(user.getOid(), user.getUsername(), user.getEmail(), user.getPassword(),
					user.getEmployeeOid(), active, user.getAccountExpired(), user.getCredentialsExpired(), user.getAccountLocked(),user.getTotalLoginAttempt(), user.getLoginAttemptCount(), user.getPhone());
			userService.create(newUser);
		} else {

			log.info("Password modified");
			Users newUser = new Users(user.getOid(), user.getUsername(), user.getEmail(), encoder.encode(userPassword),
					user.getEmployeeOid(), active, user.getAccountExpired(), user.getCredentialsExpired(), user.getAccountLocked(),user.getTotalLoginAttempt(), user.getLoginAttemptCount(), user.getPhone());
			userService.create(newUser);
		}

		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "No error", "message: user Modified");
		return response;

	}
	
	@CrossOrigin
	@RequestMapping(value = "/reset-password", method = RequestMethod.POST)
	public ResponseObject resetPassword(@RequestBody JsonNode userDetails) {
		log.info("url /reset-password");

		log.info("Request  {}", userDetails);

		if (!userDetails.has("userId") || !userDetails.has("oldPassword") || !userDetails.has("newPassword")
				|| !userDetails.has("confirmPassword")) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Invalid request", "[]");
			return response;
		}
		String userId = userDetails.get("userId").asText();
		String oldPassword = userDetails.get("oldPassword").asText();
		String newPassword = userDetails.get("newPassword").asText();
		
		
		Users user = userService.find(userId);
		
		
		
		if(user == null) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "ভুল ইউসার আইডি", new ArrayList<>());
			return response;
		}
		else if(!encoder.matches(oldPassword, user.getPassword()) ) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "পাসওয়ার্ড মিলেনি", new ArrayList<>());
			return response;
		}
		else if(newPassword.toLowerCase().contains(user.getUsername().toLowerCase())) {
			ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "পাসওয়ার্ড ইউজার আইডি, ইমেইল বা মোবাইল নম্বর থাকা যাবে না", new ArrayList<>());
			return response;
		}
		else {
						
			EmployeeRequestHeader header = new EmployeeRequestHeader("random-uuid", "portal", "portal", "grp", "random",
					new Date(), "v1", 30, 3, 10, "abc"); 

			Map<String, String> meta = new HashMap<>();

			EmployeeRequestByOidBody body = new EmployeeRequestByOidBody(user.getEmployeeOid());

			EmployeeByOid employee = new EmployeeByOid();

			employee.setHeader(header);
			employee.setMeta(meta);
			employee.setBody(body);

			RestTemplate restTemplate = new RestTemplate();

			HttpEntity<EmployeeByOid> entity = new HttpEntity<EmployeeByOid>(employee,
					createHeaders());

			ResponseEntity<JsonNode> response = null;

			try {
				response = restTemplate.exchange(hrmUrl + "/employee-personal-info/v1/get-by-employee-oid", HttpMethod.POST, entity, JsonNode.class);

			} catch (Exception e) {
				log.info("Exception in hrm");
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "employee-personal-info service threw exception",
						new ArrayList<>());
				return finalResponse;
			}
			
			
			JsonNode data = response.getBody().get("body").get("data");
			if (data.size() == 0) {
				log.info("Not Found");
				ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "No employee", new ArrayList<>());
				return finalResponse;
			}
			
			log.info("emailAddress {}" , data.get(0).get("emailAddress").asText());
			String emailAddress = data.get(0).get("emailAddress").asText();
			
			log.info("mobileNo {}" , data.get(0).get("mobileNo").asText());
			String mobileNo = data.get(0).get("mobileNo").asText();
			
			log.info("phoneNo {}", data.get(0).get("mobileNo").asText());
			String phoneNo = data.get(0).get("phoneNo").asText();
			
			log.info("name {}", data.get(0).get("nameEn").asText());
			String NameEn = data.get(0).get("nameEn").asText();
			
			String[] splitted = NameEn.split(" ");
			
			for(String s : splitted) {
				if(s.length() > 2) {
					
					if(newPassword.toLowerCase().contains(emailAddress.toLowerCase()) || newPassword.contains(phoneNo) || newPassword.contains(mobileNo) || newPassword.toLowerCase().contains(s.toLowerCase())) {
						log.info("matched with some information");
						ResponseObject finalResponse = new ResponseObject(HttpStatus.NOT_FOUND.value(), "পাসওয়ার্ড ইউজার আইডি, ইমেইল বা মোবাইল নম্বর থাকা যাবে না", new ArrayList<>());
						return finalResponse;
					}
				}
			}
			
			
			Users newUser = new Users(user.getOid(), user.getUsername(), user.getEmail(), encoder.encode(newPassword),
					user.getEmployeeOid(), user.getStatus(), user.getAccountExpired(), user.getCredentialsExpired(), user.getAccountLocked(),user.getTotalLoginAttempt(), user.getLoginAttemptCount(), user.getPhone());

			userService.create(newUser);
		}
		
		ResponseObject response = new ResponseObject(HttpStatus.OK.value(), "পাসওয়ার্ড পরিবর্তন করা হয়েছে", "message: password Modified");
		return response;
		
	}

}
