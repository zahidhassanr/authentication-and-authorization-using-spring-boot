package com.grp.cokreates.security.auth.oauth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.grp.cokreates.security.auth.entity.Office;
import com.grp.cokreates.security.auth.service.AuthWsClient;

@Component
@Order(1)
public class TransactionFilter implements Filter {
	
	@Autowired
	private AuthWsClient authClient;
	
	@Autowired
	Office office;

	@Override
    public void doFilter
	(ServletRequest request, ServletResponse response,
	FilterChain chain)throws IOException,ServletException
	{

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String officeOid = req.getHeader("officeOid");

		office.setOfficeOid(officeOid);
		
		String officeUnitOid = req.getHeader("officeUnitOid");
		
		office.setOfficeUnitOid(officeUnitOid);
		
		String officeUnitPostOid = req.getHeader("officeUnitPostOid");
		
		office.setOfficeUnitPostOid(officeUnitPostOid);
		
		String employeeOfficeOid = req.getHeader("employeeOfficeOid");
		
		office.setEmployeeOfficeOid(employeeOfficeOid);
		

		
//		if(req.getHeader("Authorization") != null && (req.getHeader("Authorization").contains("Bearer") || req.getHeader("Authorization").contains("bearer"))) {
//			
//			String authHeader = req.getHeader("Authorization");
//			
//			String token = authHeader.replace("Bearer ", "");
//			
//			String tokenRequest = "{ \"token\":\""+ token+"\"\n" + "}";
//			
//			ResponseEntity<JsonNode> checkResponse = null;
//
//			try {
//			
//				checkResponse = authClient.checkToken(tokenRequest);
//
//			} catch (Exception e) {
//				log.info("No token Found");
//				res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorised");
//				//chain.doFilter(request, res);
//				return;
//			}
//			
//			if(checkResponse.getBody().get("status").asInt() !=200) {
//				log.info("No token Found");
//				res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorised");
//			//	chain.doFilter(request, res);
//				return;
//			}
//			
//		}

		/*
		
		if(req.getHeader("Authorization") != null && (req.getHeader("Authorization").contains("Bearer") || req.getHeader("Authorization").contains("bearer"))) {
			
			String authHeader = req.getHeader("Authorization");
			
			String token = authHeader.replace("Bearer ", "");
			
			String tokenRequest = "{ \"token\":\""+ token+"\"\n" + "}";
			
			ResponseEntity<JsonNode> checkResponse = null;

			try {
			
				checkResponse = authClient.checkToken(tokenRequest);

			} catch (Exception e) {
				log.info("No token Found");
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorised");
				//chain.doFilter(request, res);
				return;
			}
			
			if(checkResponse.getBody().get("status").asInt() !=200) {
				log.info("No token Found");
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorised");
			//	chain.doFilter(request, res);
				return;
			}
			
		}
		
//		String token=req.getHeader("Authorization").replace("Bearer ","");
//		System.out.println("---Token-------"+token);
//		String apiName=req.getRequestURI().replace("/api/","");
//		System.out.println("URL"+apiName);
		
		
//		RestTemplate restTemplate = new RestTemplate();
//		  
//		  String url
//		    = "http://localhost:8085/authorize/{api}";
//		  
//		  HttpHeaders headers = new HttpHeaders();
//		 // headers.set("Authorization", "Bearer "+token);
//		  headers.setContentType(MediaType.TEXT_PLAIN);
//		  
//		   HttpEntity<String> entity = new HttpEntity<String>("Authorization: Bearer "+token, headers);
//		  
////		  Map<String, String> params = new HashMap<String, String>();
////		    params.put("api", apiName);
//		    
//		    System.out.println(entity.toString());
//		  
//		  ResponseEntity<String> response1
//		    = restTemplate.exchange(url, HttpMethod.GET, entity, String.class,apiName);
//		  System.out.println(response1);

*/
		chain.doFilter(request, response);
		
	}

	

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}


	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	

	// other methods
}
