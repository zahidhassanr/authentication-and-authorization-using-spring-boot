package com.grp.cokreates.security.auth.entity;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminByOfficeRequest {

	private EmployeeRequestHeader header;
	
	private Map<String, String> meta;
	
	private AdminByOfficeOidBody body;
}
