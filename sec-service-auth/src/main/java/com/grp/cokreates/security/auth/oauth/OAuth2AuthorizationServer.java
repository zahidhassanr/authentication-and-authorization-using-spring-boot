package com.grp.cokreates.security.auth.oauth;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import com.grp.cokreates.security.auth.config.CustomTokenEnhancer;
import com.grp.cokreates.security.auth.config.CustomTokenStore;
import com.grp.cokreates.security.auth.service.UserService;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServer extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	
	 @Autowired
	 private JdbcTemplate jdbcTemplate;
	 
	 @Autowired
     private CustomTokenStore enhancer;
	 
	 @Autowired
     private CustomTokenEnhancer tokenEnhancer;
	 

	 @Autowired
	 private UserService users;
	 
	 
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        converter.setSigningKey("ZAHID-KEY");
        KeyStoreKeyFactory keyStoreKeyFactory = 
        	      new KeyStoreKeyFactory(new ClassPathResource("mytest.jks"), "mypass".toCharArray());
        	    converter.setKeyPair(keyStoreKeyFactory.getKeyPair("mytest"));
        return converter;
    }

    /*
    
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");

			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

			keyGen.initialize(1024, random);
			KeyPair keyPair = keyGen.generateKeyPair();

			converter.setKeyPair(keyPair);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return converter;
	}

*/
    
    @Primary
	@Bean
	public JwtTokenStore jwtTokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	
	
	/*
	@Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
    */

	/*
	@Bean
	public TokenStore tokenStore() {
		
		return new JdbcTokenStore(jdbcTemplate.getDataSource());
	}
	*/
	
	/*
	  	@Bean
	    public TokenStore tokenStore() {
	        String insertAccessTokenSql = "insert into oauth_access_token (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token) values (?, ?, ?, ?, ?, ?, ?)";
//	        String selectAccessTokensFromUserNameAndClientIdSql = "select token_id, token from oauth_access_token where email = ? and client_id = ?";
//	        String selectAccessTokensFromUserNameSql = "select token_id, token from oauth_access_token where email = ?";
//	        String selectAccessTokensFromClientIdSql = "select token_id, token from oauth_access_token where client_id = ?";
	        String insertRefreshTokenSql = "insert into oauth_refresh_token (token_id, token, authentication) values (?, ?, ?)";

	        JdbcTokenStore jdbcTokenStore = new JdbcTokenStore(dataSource);
	        jdbcTokenStore.setInsertAccessTokenSql(insertAccessTokenSql);
//	        jdbcTokenStore.setSelectAccessTokensFromUserNameAndClientIdSql(selectAccessTokensFromUserNameAndClientIdSql);
//	        jdbcTokenStore.setSelectAccessTokensFromUserNameSql(selectAccessTokensFromUserNameSql);
//	        jdbcTokenStore.setSelectAccessTokensFromClientIdSql(selectAccessTokensFromClientIdSql);
	        jdbcTokenStore.setInsertRefreshTokenSql(insertRefreshTokenSql);


	        return jdbcTokenStore;
	    }

*/
	
	
/*	
	@Bean
	public ApprovalStore approvalStore() {
		return new JdbcApprovalStore(jdbcTemplate.getDataSource());
	}
*/
	
	/*
	
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    	
//    	TokenEnhancerChain chain = new TokenEnhancerChain();
//        chain.setTokenEnhancers(
//           Arrays.asList(enhancer, accessTokenConverter()));
        
        endpoints
           .authenticationManager(authenticationManager)
           .tokenStore(tokenStore())
           .approvalStore(approvalStore())
           .accessTokenConverter(accessTokenConverter());
        


    }
    
    */
    
   

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		
		
		 TokenEnhancerChain chain = new TokenEnhancerChain();
         chain.setTokenEnhancers(
         Arrays.asList(tokenEnhancer, accessTokenConverter()));
		
		endpoints.authenticationManager(authenticationManager)
				.tokenEnhancer(chain)
				.tokenStore(enhancer)
				.accessTokenConverter(accessTokenConverter())
				.authenticationManager(authenticationManager).userDetailsService(users);
		
		
		
		
	}

	/*
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.approvalStore(approvalStore()).tokenStore(tokenStore());
	}
	
	*/
	

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("permitAll()");
		
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		/*
		clients.inMemory().withClient("clientapp").secret(encoder.encode("123456")).scopes("read_profile")
				.authorizedGrantTypes("password", "authorization_code", "refresh_token");
				
		*/
		
		clients
        .jdbc(jdbcTemplate.getDataSource());
	}
	
//	@Bean
//	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
//	public Office requestScopedBean() {
//	    return new Office();
//	}

}
