package com.grp.cokreates.security.auth.config;


import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import com.grp.cokreates.security.auth.entity.AccessToken;
import com.grp.cokreates.security.auth.service.AccessTokenService;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
public class CustomTokenStore implements TokenStore {
	
	
	
	
	
	@Autowired
	private AccessTokenService accService;
	

	
	private AuthenticationKeyGenerator authenticationKeyGenerator = new DefaultAuthenticationKeyGenerator();
	
	
	
	@Override
	public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
		log.info("it came here readAuthentication");
		return null;
	}

	@Override
	public OAuth2Authentication readAuthentication(String token) {
	
		log.info("it came here readAuthentication2");
		return null;
	}

	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		/*
		 
		java.sql.Date expire =new java.sql.Date(token.getExpiration().getTime());    //convert normal date to sql date
		Jwt jwtToken = JwtHelper.decode(token.toString());    //Decode jwt token
	    String claims = jwtToken.getClaims();										//return jwt token in string format
	   
	   */
		
		
		
	    log.info("it came here storeAccessToken");
	    
	    log.info("Expiration time of token {}",System.currentTimeMillis()/1000+token.getExpiresIn());
	    
	    //Long expire = System.currentTimeMillis()/1000+token.getExpiresIn();
	    
		
		accService.create(new AccessToken(UUID.randomUUID().toString()+UUID.randomUUID().toString(), extractTokenKey(token.getValue()), token.getValue(), authenticationKeyGenerator.extractKey(authentication), authentication.getName(), authentication.getOAuth2Request().getClientId(), authentication, token.getRefreshToken().getValue()));
		
	}

	@Override
	public OAuth2AccessToken readAccessToken(String tokenValue) {
		
		log.info("it came here readAccessToken");
		return null;
	}

	@Override
	public void removeAccessToken(OAuth2AccessToken token) {
		
		log.info("it came here removeAccessToken");
		
	}

	@Override
	public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
		
		log.info("it came here storeRefreshToken");

		/*
		String username=authentication.getName();
		System.out.println("store refresh user"+username);
		
		List<RefreshToken> tokens=refService.findByUserName(username);
	    
	    if(tokens!= null)
	    {
	    	refService.deleteByUser(username);
	    }
		
		refService.create(new RefreshToken(UUID.randomUUID().toString()+UUID.randomUUID().toString(),extractTokenKey(refreshToken.getValue()), refreshToken.getValue(),authentication.getName(), authentication));
		*/
	}

	@Override
	public OAuth2RefreshToken readRefreshToken(String tokenValue) {
		
		log.info("It came here readRefreshToken and token is {}",tokenValue);
		
		log.info("After {}",new DefaultOAuth2RefreshToken(tokenValue));
		
		return new DefaultOAuth2RefreshToken(tokenValue);
	}

	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
	
		log.info("it came here readAuthenticationForRefreshToken with token {}", token.getValue());
			
		AccessToken at = accService.findByRefreshToken(token.getValue());
		
	
		return at.getAuthentication();
		
	
	}

	@Override
	public void removeRefreshToken(OAuth2RefreshToken token) {
		log.info("it came here removeRefreshToken");
		
		
	}

	@Override
	public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
		log.info("it came here removeAccessTokenUsingRefreshToken");
	
		
		AccessToken token = accService.findByRefreshToken(refreshToken.getValue());
		
		accService.delete(token);
		
	}

	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
		
		log.info("it came here getAccessToken");
		return null;
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
	
		log.info("it came here findTokensByClientIdAndUserName");
		return null;
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {

		log.info("it came here findTokensByClientId");
		return null;
	}
	

	private String extractTokenKey(String value) {
        if(value == null) {
            return null;
        } else {
            MessageDigest digest;
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException var5) {
                throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
            }
 
            try {
                byte[] e = digest.digest(value.getBytes("UTF-8"));
                return String.format("%032x", new Object[]{new BigInteger(1, e)});
            } catch (UnsupportedEncodingException var4) {
                throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).");
            }
        }
    }

}
