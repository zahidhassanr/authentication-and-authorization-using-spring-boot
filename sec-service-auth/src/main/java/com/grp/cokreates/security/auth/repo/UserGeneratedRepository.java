package com.grp.cokreates.security.auth.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import com.grp.cokreates.security.auth.entity.UserGeneratedString;

public interface UserGeneratedRepository extends JpaRepository<UserGeneratedString, String>{
	
	@Modifying
	void deleteByGeneratedString(String generated);
	
	UserGeneratedString findByGeneratedString(String generated);
}
