package com.grp.cokreates.security.auth.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity
@Table(name = "grp_user", schema = "sec")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users implements Serializable{

	@Id
	@Column(name="oid")
	private String oid;
	
	
	@Column(name="username")
	private String username;
	
	@Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name="employee_oid")
	private String employeeOid;

	@Column(name="status")
	private String status;
	
	@Column(name="account_expired")
	private String accountExpired;
	
	@Column(name="credentials_expired")
	private String credentialsExpired;
	
	@Column(name="account_locked")
	private String accountLocked;
	
	@Column(name="total_login_attempt")
	private int totalLoginAttempt;
	
	@Column(name="login_attempt_count")
	private int loginAttemptCount;
	
	@Column(name="phone")
	private String phone;
}

