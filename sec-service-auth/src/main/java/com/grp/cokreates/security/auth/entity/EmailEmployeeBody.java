package com.grp.cokreates.security.auth.entity;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailEmployeeBody {

	private String empId;
    private String orgId;
    private String emailAddress;
    private String recipientType;
}
