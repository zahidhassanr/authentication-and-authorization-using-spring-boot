package com.grp.cokreates.security.auth.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.grp.cokreates.security.auth.entity.Users;

@Repository
public interface UserRepository extends CrudRepository<Users, String> {
	Users findByUsername(String uname);
	
	Users findByEmail(String userid);
	
	Users findByOid(String oid);
	
	@Query("select u from Users u where u.username like :userid%")
    List<Users> findByString(@Param(value="userid") String userid);
	
	Users findByEmployeeOid(String oid);
	
	
	List<Users> findByEmployeeOidIn(List<String> employeeOids);
	
	List<Users> findByOidIn(List<String> Oids);
	
	Users findByPhone(String phone);
	
}

