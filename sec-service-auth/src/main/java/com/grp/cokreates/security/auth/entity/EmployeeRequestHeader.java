package com.grp.cokreates.security.auth.entity;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequestHeader {
	@NotBlank
	private String requestId;
	
	@NotBlank
	private String requestSource;
	
	@NotBlank
	private String requestSourceService;
	
	@NotBlank
	private String requestClient;
	
	@NotBlank
	private String requestType;
	
	@NotNull
	private Date requestTime;
	
	@NotBlank
	private String requestVersion;
	
	@NotNull
	private Integer requestTimeoutInSeconds;
	
	@NotNull
	@Min(0)
	private Integer requestRetryCount;
	
	@Min(0)
	private Integer hopCount;
	
	private String traceId;
}
