package com.grp.cokreates.security.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsEmployeeBody {

	private String empId;
	private String orgId;
	private String contactNo;

}
