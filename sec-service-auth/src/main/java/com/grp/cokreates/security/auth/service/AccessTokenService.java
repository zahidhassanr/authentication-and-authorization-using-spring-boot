package com.grp.cokreates.security.auth.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grp.cokreates.security.auth.entity.AccessToken;
import com.grp.cokreates.security.auth.repo.AccessTokenRepo;


@Service
@Transactional
public class AccessTokenService {
	
	@Autowired
	private AccessTokenRepo accessRepo;
	
	
	public AccessToken findByToken(String token)
	{
		return accessRepo.findByToken(token);
	}
	
	public AccessToken findByRefreshToken(String refreshToken)
	{
		return accessRepo.findRefreshToken(refreshToken);
	}


//    public AccessToken findByAuthenticationId(String authenticationId)
//    {
//    	return accessRepo.FindAuthenticationId(authenticationId);
//    }
    
    
    public List<AccessToken> findByUserName(String username)
    {
    	return accessRepo.FindByUserName(username);
    }
    
    public void create(AccessToken a)
    {
    	accessRepo.save(a);
    	
    }
    
    public void deleteByUser(String userid)
    {
    	accessRepo.deleteByUserId(userid);
    }
    
    public void deleteByToken(String token)
    {
    	accessRepo.deleteByToken(token);
    }
    
    public void delete(AccessToken token)
    {
    	accessRepo.delete(token);
    }
    

}

