package com.grp.cokreates.security.auth.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailRequestBody {
	private String oid;	
	private String token;
	private String tag;
	private List<EmailEmployeeBody> listOfEmployees;
	private List<String> listOfGuests;
	private String subject;
	private String message;
	private String scheduledTime;
	private String hasAttachment;
}
