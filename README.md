**Authentication-and-Authorization-Usig-Spring-Boot**

This project was built for Authentication and Authorizaion of a large ERP software.

This project is built with Spring Boot framework. For authentication and authorization oAuth2 library was used along with JWT for token management.

There are two part of the project. sec-service-auth project is for authentication and authorization server where validation and auhrization take place and sec-service-resource project is like a resource server with client apis which use sec-service-auth for authentication and authorization.
